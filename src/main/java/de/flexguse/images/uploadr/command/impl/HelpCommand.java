/**
 * 
 */
package de.flexguse.images.uploadr.command.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import de.flexguse.images.uploadr.command.UploadrCommand;
import de.flexguse.images.uploadr.command.UploadrCommandException;

/**
 * This command prints the help information to System.out. The help information
 * is stored in resources/commands/help.txt.
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public class HelpCommand extends AbstractCommand implements UploadrCommand {
	
	@Override
	public void setUploadrArguments(String[] uploadrArguments)
			throws UploadrCommandException {
		// intended to do nothing
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.flexguse.images.uploadr.command.UploadrCommand#execute()
	 */
	@Override
	public void execute() throws UploadrCommandException {

		BufferedReader ir = null;
		try {

			ir = new BufferedReader(new InputStreamReader(getClass()
					.getResourceAsStream("/commands/help.txt")));
			String str;
			while ((str = ir.readLine()) != null) {
				System.out.println(str);
			}

		} catch (IOException e) {
			throw new UploadrCommandException(e);
		} finally {
			if (ir != null) {
				try {
					ir.close();
				} catch (IOException e) {
					// intended to do nothing
				}
			}
		}

	}

}
