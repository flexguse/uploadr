/**
 * 
 */
package de.flexguse.images.uploadr.command;

import de.flexguse.images.uploadr.service.Uploadr;

/**
 * A command is a functionality of the Uploadr (i.e. upload, uploadlog, info).
 * Ensure all needed information is given in the constructor of the
 * {@link UploadrCommand}.
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public interface UploadrCommand {

	/**
	 * Sets the command line arguments given in the {@link Uploadr} call.
	 */
	public void setUploadrArguments(String[] arguments) throws UploadrCommandException;
	
	/**
	 * This method executes the Uploadr command. Ensure in the implementation
	 * that long running operations are executed only once.
	 * 
	 * @throws UploadrCommandException
	 */
	public void execute() throws UploadrCommandException;

}
