/**
 * 
 */
package de.flexguse.images.uploadr.command.impl;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import org.springframework.util.DefaultPropertiesPersister;
import org.springframework.util.PropertiesPersister;

import de.flexguse.images.uploadr.command.UploadrCommand;
import de.flexguse.images.uploadr.command.UploadrCommandException;
import de.flexguse.images.uploadr.service.ExternalUploadr;
import de.flexguse.images.uploadr.service.LockInformationService;

/**
 * This abstract command contains members and attributes which are needed by all
 * {@link UploadrCommand}s.
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public abstract class AbstractCommand implements ApplicationContextAware {

	@Autowired(required = false)
	protected LockInformationService lockInformationService;

	/**
	 * The Spring application context to be able to get beans from the context.
	 */
	protected ApplicationContext springContext;

	/**
	 * The property file which holds configuration for the command.
	 */
	protected Properties configurationProperties;

	/**
	 * The name of the configuration file. Needed as unique key.
	 */
	protected String configurationFilename;

	/**
	 * The command line arguments given by the user when starting the command.
	 */
	protected String[] uploadrArguments;

	protected static final String FLICKR_UPLOADR_PREFIX = "flickr";
	
	protected FuzzyExternalUploadrHashMap externalUploadrMap;

	/**
	 * Sets the map which is used to get the {@link ExternalUploadr} for the
	 * configuration name.
	 * 
	 * @param externalUploadrMap
	 */
	public void setExternalUploadrMap(
			FuzzyExternalUploadrHashMap externalUploadrMap) {
		this.externalUploadrMap = externalUploadrMap;
	}
	

	public void setLockInformationService(
			LockInformationService lockInformationService) {
		this.lockInformationService = lockInformationService;
	}

	public void setUploadrArguments(String[] uploadrArguments)
			throws UploadrCommandException {
		this.uploadrArguments = uploadrArguments;

		// check if configuration file is given
		configurationFilename = getArgument(1, uploadrArguments);
		if (configurationFilename != null) {

			configurationProperties = loadConfigurationFile(configurationFilename);

		} else {
			throw new UploadrCommandException("no configuration file given");
		}

	}

	/**
	 * This sets the uploadrArguments without validation if the configuration
	 * file exists. Use this method for testing purposes only!
	 * 
	 * @param uploadrArguments
	 */
	public void setUploadrArgumentsQuietly(String[] uploadrArguments) {
		this.uploadrArguments = uploadrArguments;
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext)
			throws BeansException {
		this.springContext = applicationContext;

	}

	public void setConfigurationFilename(String configurationFilename) {
		this.configurationFilename = configurationFilename;
	}

	/**
	 * This helper method loads the configuration file from the classpath.
	 * 
	 * @param configurationFilename
	 * @return Properties from the configuration file
	 * @throws UploadrCommandException
	 *             if the configuration file was not found
	 */
	protected Properties loadConfigurationFile(String configurationFilename)
			throws UploadrCommandException {

		Resource resource = new ClassPathResource(configurationFilename);

		try {
			return PropertiesLoaderUtils.loadProperties(resource);
		} catch (IOException e) {
			throw new UploadrCommandException(e);
		}

	}

	/**
	 * This helper method persists the given properties in the configuration
	 * file.
	 * 
	 * @param configurationFilename
	 * @param updatedProperties
	 * @throws UploadrCommandException
	 */
	protected void storePropertiesInConfigurationFile(
			String configurationFilename, Properties updatedProperties)
			throws UploadrCommandException {

		Resource resource = new ClassPathResource(configurationFilename);
		try {
			File propertiesFile = resource.getFile();
			FileWriter fileWriter = new FileWriter(propertiesFile);
			PropertiesPersister propertiesPersister = new DefaultPropertiesPersister();
			propertiesPersister.store(updatedProperties, fileWriter, null);

		} catch (IOException e) {
			throw new UploadrCommandException(e);
		}

	}

	/**
	 * This helper method gets the n-th argument from the given arguments.
	 * 
	 * @param argumentNumber
	 *            counting starts with 0
	 * @param args
	 * @return null or argument
	 */
	protected String getArgument(int argumentNumber, String[] args) {

		if (args != null && args.length >= argumentNumber + 1) {
			return args[argumentNumber];
		}

		return null;

	}

}
