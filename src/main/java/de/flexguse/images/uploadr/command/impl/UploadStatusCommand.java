/**
 * 
 */
package de.flexguse.images.uploadr.command.impl;

import de.flexguse.images.uploadr.command.UploadrCommand;
import de.flexguse.images.uploadr.command.UploadrCommandException;
import de.flexguse.images.uploadr.model.LockInformation;

/**
 * This command shows the information which commands are currently running and
 * marked as locked. The information is printed to sysout.
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public class UploadStatusCommand extends AbstractCommand implements
		UploadrCommand {
	
	@Override
	public void setUploadrArguments(String[] uploadrArguments)
			throws UploadrCommandException {
		// intended to do nothing
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see de.flexguse.images.uploadr.command.UploadrCommand#execute()
	 */
	@Override
	public void execute() throws UploadrCommandException {
		
		printHeader();
		for(LockInformation lockInformation : lockInformationService.getAllLockInformations()){
			printLockInformation(lockInformation);
		}
		
	}

	/**
	 * This helper method prints the header.
	 */
	private void printHeader() {

		System.out
				.println("* Command start date 			* Command configuration filename			* Command name");
		System.out
				.println("==============================================================================================================");

	}

	/**
	 * This helper method prints a single {@link LockInformation}.
	 * 
	 * @param lockInformation
	 */
	private void printLockInformation(LockInformation lockInformation) {
		StringBuilder line = new StringBuilder();
		line.append(lockInformation.getStartTime()).append(" * ")
				.append(lockInformation.getConfigurationFile()).append(" * ")
				.append(lockInformation.getCommand());
		System.out.println(line.toString());
	}

}
