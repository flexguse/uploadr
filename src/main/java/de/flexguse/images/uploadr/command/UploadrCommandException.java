/**
 * 
 */
package de.flexguse.images.uploadr.command;

/**
 * @author Christoph Guse, info@flexguse.de
 *
 */
public class UploadrCommandException extends Exception {

	private static final long serialVersionUID = -7208443963571727430L;

	public UploadrCommandException(String message) {
		super(message);
	}
	
	public UploadrCommandException(Exception e){
		super(e);
	}
	
}
