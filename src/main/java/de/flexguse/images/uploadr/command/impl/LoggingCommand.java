/**
 * 
 */
package de.flexguse.images.uploadr.command.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import de.flexguse.images.uploadr.command.UploadrCommand;
import de.flexguse.images.uploadr.command.UploadrCommandException;
import de.flexguse.images.uploadr.model.ActionLog;
import de.flexguse.images.uploadr.service.ActionLogService;

/**
 * The {@link LoggingCommand} is able to create logs from {@link ActionLog}s.
 * <ul>
 * <li>first parameter: configuration filename (optional)</li>
 * <li>second parameter: date which is for between (optional). second date is
 * always current date. date format is mm-dd-yyyy</li>
 * </ul>
 * If no parameters given all {@link ActionLog}s are logged for the last 2
 * days.
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public class LoggingCommand extends AbstractCommand implements UploadrCommand {

	private SimpleDateFormat simpleDateFormat;

	@Autowired(required = false)
	private ActionLogService uploadResultService;

	public LoggingCommand() {
		super();

		simpleDateFormat = new SimpleDateFormat("MM-dd-yyyy");
	}

	@Override
	public void setUploadrArguments(String[] uploadrArguments)
			throws UploadrCommandException {
		this.uploadrArguments = uploadrArguments;
	}

	public void setUploadResultService(ActionLogService uploadResultService) {
		this.uploadResultService = uploadResultService;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.flexguse.images.uploadr.command.UploadrCommand#execute()
	 */
	@Override
	public void execute() throws UploadrCommandException {

		String configurationFilename = null;

		// default to-date
		Date toDate = new Date();

		// default from-date
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -2);
		Date fromDate = cal.getTime();

		// get arguments
		String firstArgument = getArgument(1, uploadrArguments);
		String secondArgument = getArgument(2, uploadrArguments);

		// try to get date from arguments
		Date givenFromDate = null;
		if (StringUtils.isNotBlank(firstArgument)) {
			givenFromDate = getDate(firstArgument);
			if(givenFromDate != null){
				fromDate = givenFromDate;
			}
			if (givenFromDate == null) {
				configurationFilename = firstArgument;

				if (StringUtils.isNotBlank(secondArgument)) {
					givenFromDate = getDate(secondArgument);
				}

			}
		}

		// get number of matching upload results
		long numberOfMatchingUploadResults = uploadResultService
				.countUploadResults(fromDate, toDate, configurationFilename);
		int numberOfResultsPerPage = 20;
		int position = 0;
		while (numberOfMatchingUploadResults >= position
				* numberOfResultsPerPage) {
			printUploadResults(uploadResultService.getActionLogs(fromDate,
					toDate, configurationFilename, position,
					numberOfResultsPerPage));
			position=position+1;
		}

	}

	/**
	 * This helper method prints a list of upload result objects.
	 * 
	 * @param uploadResults
	 */
	private void printUploadResults(List<ActionLog> uploadResults) {
		if (uploadResults != null) {

			for (ActionLog uploadResult : uploadResults) {
				printUploadResult(uploadResult);
			}
		}

	}

	/**
	 * This helper method tries to parse a date from the given argument.
	 * 
	 * @param arg
	 * @return
	 */
	private Date getDate(String arg) {

		if (StringUtils.isNotBlank(arg)) {
			try {
				return simpleDateFormat.parse(arg);
			} catch (ParseException e) {
				// intended to do nothing, argument simply is no date
			}
		}

		return null;

	}

	/**
	 * This helper method prints a single {@link ActionLog} to the console
	 * output.
	 * 
	 * @param actionLog
	 */
	private void printUploadResult(ActionLog actionLog) {

		StringBuilder out = new StringBuilder();
		out.append(actionLog.getAction()).append(" ").append(actionLog.isActionSuccessful()).append(" ")
				.append(actionLog.getActionStartTime()).append(" ")
				.append(actionLog.getActionDurationInSeconds()).append("s ")
				.append(actionLog.getConfigurationFileName()).append(" ")
				.append(actionLog.getFolderName()).append(" ")
				.append(actionLog.getFilename()).append(" ")
				.append(actionLog.getRemoteServiceResponse());
		System.out.println(out.toString());

	}

}
