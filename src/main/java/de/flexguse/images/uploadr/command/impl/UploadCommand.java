/**
 * 
 */
package de.flexguse.images.uploadr.command.impl;

import org.apache.commons.lang3.StringUtils;

import de.flexguse.images.uploadr.command.UploadrCommand;
import de.flexguse.images.uploadr.command.UploadrCommandException;
import de.flexguse.images.uploadr.service.AlreadyLockedException;
import de.flexguse.images.uploadr.service.ExternalUploadr;
import de.flexguse.images.uploadr.service.UploadrService;
import de.flexguse.images.uploadr.service.flickr.FlickrUploadr;

/**
 * The {@link UploadrCommand} is able to upload content to several targets. It
 * uses the given configuration file to configure the external uploadr.
 * <p>
 * Which uploadr is used is determined by the configuration filename. Start with
 * <ul>
 * <li>'flickr': the {@link FlickrUploadr} is used</li>
 * </ul>
 * </p>
 * 
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public class UploadCommand extends AbstractCommand implements UploadrCommand {

	private static final String COMMAND_NAME = "upload";

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.flexguse.images.uploadr.command.UploadrCommand#execute()
	 */
	@Override
	public void execute() throws UploadrCommandException {

		if (lockInformationService
				.isLocked(COMMAND_NAME, configurationFilename)) {
			throw new UploadrCommandException(
					"UploadCommand is already running for "
							+ configurationFilename);
		} else {
			try {
				lockInformationService.lockCommand(COMMAND_NAME,
						configurationFilename);

				try {
					ExternalUploadr externalUploadr = null;
					UploadrService uploadrService = springContext
							.getBean(UploadrService.class);

					externalUploadr = externalUploadrMap
							.get(configurationFilename);

					if (externalUploadr == null) {
						throw new UploadrCommandException(
								"No external uploadr registered for '"
										+ configurationFilename + "'");
					}

					if (externalUploadr.configure(configurationProperties) != null) {
						/*
						 * configuration properties must be rewritten in case
						 * the externalUploader modified them, i.e. the
						 * FlickrUploader did the last OAuth step.
						 */
						storePropertiesInConfigurationFile(
								configurationFilename, configurationProperties);
					}

					uploadrService.setExternalUploadr(externalUploadr);

					String basefolders = configurationProperties
							.getProperty("basefolders");
					if (StringUtils.isNotBlank(basefolders)) {
						String[] basefolderList = StringUtils.split(
								basefolders, ",");
						for (String baseFolder : basefolderList) {
							if (StringUtils.isNotBlank(baseFolder)) {
								uploadrService.scanAndUpload(
										configurationFilename, baseFolder);
							} else {
								throw new UploadrCommandException(
										"basefolders must not be empty");
							}

						}

					} else {
						throw new UploadrCommandException(
								"no upload basefolders given");
					}
				} catch (Exception e) {
					throw e;
				} finally {
					lockInformationService.unlockCommand(COMMAND_NAME,
							configurationFilename);
				}

			} catch (AlreadyLockedException e) {
				throw new UploadrCommandException(e);
			}

		}

	}

}
