/**
 * 
 */
package de.flexguse.images.uploadr.command.impl;

import org.apache.commons.lang3.StringUtils;

import de.flexguse.images.uploadr.command.UploadrCommand;
import de.flexguse.images.uploadr.command.UploadrCommandException;
import de.flexguse.images.uploadr.service.Uploadr;

/**
 * The {@link UnlockCommand} is able to unlock a running {@link Uploadr}
 * command.
 * 
 * <p>
 * First constructor argument is the configuration filename, second argument is
 * the command name.
 * </p>
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public class UnlockCommand extends AbstractCommand implements UploadrCommand {
	
	@Override
	public void execute() throws UploadrCommandException {

		String commandName = getArgument(2, uploadrArguments);

		if (StringUtils.isBlank(commandName)) {
			throw new UploadrCommandException(
					"Missing command name. For unlocking command the configuration filename and the command name is needed.");
		}

		if (lockInformationService.isLocked(commandName, configurationFilename)) {
			// unlock
			lockInformationService.unlockCommand(commandName,
					configurationFilename);
		} else {
			throw new UploadrCommandException("The command '" + commandName
					+ "' for the configuration file '" + configurationFilename
					+ "' is not locked and can't be unlocked.");
		}

	}

}
