/**
 * 
 */
package de.flexguse.images.uploadr.command.impl;

import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

import de.flexguse.images.uploadr.command.UploadrCommand;
import de.flexguse.images.uploadr.command.UploadrCommandException;
import de.flexguse.images.uploadr.service.ExternalUploadr;
import de.flexguse.images.uploadr.service.flickr.FlickrUploadr;

/**
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public class OAuthCommand extends AbstractCommand implements UploadrCommand {

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.flexguse.images.uploadr.command.UploadrCommand#execute()
	 */
	@Override
	public void execute() throws UploadrCommandException {

		ExternalUploadr externalUploadr = null;
		
		if (StringUtils.contains(configurationFilename, FLICKR_UPLOADR_PREFIX)) {			
			externalUploadr = springContext.getBean(FlickrUploadr.class);
		}
		
		externalUploadr.configure(loadConfigurationFile(configurationFilename));
		
		Map<String, Object> oAuthMap = externalUploadr.doOAuthentication();
		
		// write map content to properties file
		Properties properties = loadConfigurationFile(configurationFilename);
		
		Set<String> keys= oAuthMap.keySet();
		for(String key : keys){
			properties.put(key, oAuthMap.get(key));
		}
		
		storePropertiesInConfigurationFile(configurationFilename, properties);
		
		

	}

}
