/**
 * 
 */
package de.flexguse.images.uploadr.command.impl;

import java.util.HashMap;

import org.apache.commons.lang3.StringUtils;

import de.flexguse.images.uploadr.service.ExternalUploadr;

/**
 * This very simple implementation of a Map provides functionality to find
 * entries with fuzzy keys. The implementation is not performance optimized and
 * should not be used with many entries.
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public class FuzzyExternalUploadrHashMap extends
		HashMap<String, ExternalUploadr> {

	private static final long serialVersionUID = -7407985723750946695L;

	/**
	 * Sweeps through all keys and does a String.contains for get matching key.
	 */
	public ExternalUploadr get(String key) {

		if (StringUtils.isNotBlank(key)) {
			for (String containedKey : keySet()) {
				if (StringUtils.contains(key,containedKey)) {
					return super.get(containedKey);
				}
			}
		}

		return super.get(key);
	}

}
