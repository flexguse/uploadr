/**
 * 
 */
package de.flexguse.images.uploadr.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.AbstractPersistable;

/**
 * The ActionLog contains the result of any action in the Uploadr.
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
@Table(name = "uploadr_actionlog")
@Entity
public class ActionLog extends AbstractPersistable<Long> {

	private static final long serialVersionUID = 5182979465026226550L;

	private Date actionStartTime = new Date();

	// the duration of the upload in seconds
	private Long actionDurationInSeconds = 0L;

	private boolean actionSuccessful = false;

	private String action;

	@Column(length = 400)
	private String filename;

	@Column(length = 4000)
	private String folderName;

	@Column(length = 400)
	private String configurationFileName;

	@Lob
	private String remoteServiceResponse;

	public Date getActionStartTime() {
		return actionStartTime;
	}

	public void setActionStartTime(Date uploadStartTime) {
		this.actionStartTime = uploadStartTime;
	}

	public Long getActionDurationInSeconds() {
		return actionDurationInSeconds;
	}

	public void setActionDurationInSeconds(Long actionDurationInSeconds) {
		this.actionDurationInSeconds = actionDurationInSeconds;
	}

	public void setActionDurationInMilliseconds(Long millisecondDuration) {

		if (millisecondDuration != null) {
			setActionDurationInSeconds(millisecondDuration / 1000L);
		}
	}

	public boolean isActionSuccessful() {
		return actionSuccessful;
	}

	public void setActionSuccessful(boolean actionSuccessful) {
		this.actionSuccessful = actionSuccessful;
	}

	/**
	 * Gets the action (i.e. upload, delete etc.).
	 * 
	 * @return
	 */
	public String getAction() {
		return action;
	}

	/**
	 * Sets the action (i.e. upload, delete etc.).
	 * 
	 * @param action
	 */
	public void setAction(String action) {
		this.action = action;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getFolderName() {
		return folderName;
	}

	public void setFolderName(String folderName) {
		this.folderName = folderName;
	}

	public String getConfigurationFileName() {
		return configurationFileName;
	}

	public void setConfigurationFileName(String configurationFileName) {
		this.configurationFileName = configurationFileName;
	}

	public String getRemoteServiceResponse() {
		return remoteServiceResponse;
	}

	public void setRemoteServiceResponse(String remoteServiceResponse) {
		this.remoteServiceResponse = remoteServiceResponse;
	}

	/**
	 * This method puts all relevant information from the {@link UploadedFile}
	 * into the {@link ActionLog}.
	 * 
	 * @param uploadedFile
	 */
	public void fillFromUploadedFile(UploadedFile uploadedFile) {

		if (uploadedFile != null) {

			setRemoteServiceResponse(uploadedFile.getActionServiceResponse());
			setConfigurationFileName(uploadedFile.getConfigurationFilename());
			setFilename(uploadedFile.getFilename());
			setFolderName(uploadedFile.getFilePath());
			setActionSuccessful(uploadedFile.isUploadSuccessful());
		}

	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((action == null) ? 0 : action.hashCode());
		result = prime
				* result
				+ ((actionDurationInSeconds == null) ? 0
						: actionDurationInSeconds.hashCode());
		result = prime * result
				+ ((actionStartTime == null) ? 0 : actionStartTime.hashCode());
		result = prime * result + (actionSuccessful ? 1231 : 1237);
		result = prime
				* result
				+ ((configurationFileName == null) ? 0 : configurationFileName
						.hashCode());
		result = prime * result
				+ ((filename == null) ? 0 : filename.hashCode());
		result = prime * result
				+ ((folderName == null) ? 0 : folderName.hashCode());
		result = prime
				* result
				+ ((remoteServiceResponse == null) ? 0 : remoteServiceResponse
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ActionLog other = (ActionLog) obj;
		if (action == null) {
			if (other.action != null)
				return false;
		} else if (!action.equals(other.action))
			return false;
		if (actionDurationInSeconds == null) {
			if (other.actionDurationInSeconds != null)
				return false;
		} else if (!actionDurationInSeconds
				.equals(other.actionDurationInSeconds))
			return false;
		if (actionStartTime == null) {
			if (other.actionStartTime != null)
				return false;
		} else if (!actionStartTime.equals(other.actionStartTime))
			return false;
		if (actionSuccessful != other.actionSuccessful)
			return false;
		if (configurationFileName == null) {
			if (other.configurationFileName != null)
				return false;
		} else if (!configurationFileName.equals(other.configurationFileName))
			return false;
		if (filename == null) {
			if (other.filename != null)
				return false;
		} else if (!filename.equals(other.filename))
			return false;
		if (folderName == null) {
			if (other.folderName != null)
				return false;
		} else if (!folderName.equals(other.folderName))
			return false;
		if (remoteServiceResponse == null) {
			if (other.remoteServiceResponse != null)
				return false;
		} else if (!remoteServiceResponse.equals(other.remoteServiceResponse))
			return false;
		return true;
	}

}
