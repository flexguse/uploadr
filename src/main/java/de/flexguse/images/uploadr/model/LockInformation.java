package de.flexguse.images.uploadr.model;


import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Index;
import org.springframework.data.jpa.domain.AbstractPersistable;

/**
 * This entity stores information if a command is already running. Primary keys
 * are the command name and the configuration filename.
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
@Entity
public class LockInformation extends AbstractPersistable<Long> {

	private static final long serialVersionUID = -4930411531850587011L;

	@Index(name="commandName")
	private String command;

	@Lob
	private String configurationFile;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date startTime = new Date();

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public String getConfigurationFile() {
		return configurationFile;
	}

	public void setConfigurationFile(String configurationFile) {
		this.configurationFile = configurationFile;
	}
	
	public Date getStartTime() {
		return startTime;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((command == null) ? 0 : command.hashCode());
		result = prime
				* result
				+ ((configurationFile == null) ? 0 : configurationFile
						.hashCode());
		result = prime * result
				+ ((startTime == null) ? 0 : startTime.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		LockInformation other = (LockInformation) obj;
		if (command == null) {
			if (other.command != null)
				return false;
		} else if (!command.equals(other.command))
			return false;
		if (configurationFile == null) {
			if (other.configurationFile != null)
				return false;
		} else if (!configurationFile.equals(other.configurationFile))
			return false;
		if (startTime == null) {
			if (other.startTime != null)
				return false;
		} else if (!startTime.equals(other.startTime))
			return false;
		return true;
	}



}
