/**
 * 
 */
package de.flexguse.images.uploadr.model;

import java.io.File;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.Index;
import org.springframework.data.jpa.domain.AbstractPersistable;

/**
 * A {@link UploadedFile} is the equivalent to the files in the remote system.
 * If an UploadedFile exists, the file was uploaded.
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
@Table(name = "uploadr_monitored_files")
@Entity
public class UploadedFile extends AbstractPersistable<Long> {

	private static final long serialVersionUID = 6825058163930397816L;

	@Column(length = 400)
	@Index(name = "uploadr_configurationfilenameIndex")
	protected String configurationFilename;

	@Column(length = 400)
	@Index(name = "uploadr_filenameIndex")
	protected String filename;

	@Column(length = 4000)
	@Index(name = "uploadr_filePathIndex")
	protected String filePath;

	protected String md5hash;

	@Column(length=1000)
	protected String remoteId;
	
	// the response of the service which was used for uploading
	@Transient
	private String actionServiceResponse;
	
	@Transient
	private boolean uploadSuccessful = false;

	public String getConfigurationFilename() {
		return configurationFilename;
	}

	public void setConfigurationFilename(String configurationFilename) {
		this.configurationFilename = configurationFilename;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		
		if(StringUtils.isNotBlank(filePath)){
			
			// the filepath must be converted to remove all system specific file separators
			filePath = new File(filePath).toURI().toString();			
		}
		
		this.filePath = filePath;
	}

	public String getMd5hash() {
		return md5hash;
	}

	public void setMd5hash(String md5hash) {
		this.md5hash = md5hash;
	}

	public String getRemoteId() {
		return remoteId;
	}

	public void setRemoteId(String remoteId) {
		this.remoteId = remoteId;
	}

	public String getActionServiceResponse() {
		return actionServiceResponse;
	}

	public void setActionServiceResponse(String actionServiceResponse) {
		this.actionServiceResponse = actionServiceResponse;
	}

	public boolean isUploadSuccessful() {
		return uploadSuccessful;
	}

	public void setUploadSuccessful(boolean uploadSuccessful) {
		this.uploadSuccessful = uploadSuccessful;
	}
	
	

}
