/**
 * 
 */
package de.flexguse.images.uploadr.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;

import com.mysema.query.types.expr.BooleanExpression;

import de.flexguse.images.uploadr.model.ActionLog;
import de.flexguse.images.uploadr.model.QActionLog;
import de.flexguse.images.uploadr.repository.ActionLogRepository;
import de.flexguse.images.uploadr.service.ActionLogService;

/**
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public class ActionLogServiceImpl implements ActionLogService {

	@Autowired(required = false)
	private ActionLogRepository repository;

	public void setRepository(ActionLogRepository repository) {
		this.repository = repository;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.flexguse.images.uploadr.service.UploadResultService#findUploadResult
	 * (java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public ActionLog findUploadResult(String configurationFilename,
			String fileName, String filePath) {

		return repository.findOneByConfigurationFileNameAndFilenameAndFolderName(
				configurationFilename, fileName, filePath);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.flexguse.images.uploadr.service.UploadResultService#save(de.flexguse
	 * .images.uploadr.model.UploadResult)
	 */
	@Override
	public void save(ActionLog uploadResult) {

		if (uploadResult != null) {
			repository.save(uploadResult);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.flexguse.images.uploadr.service.UploadResultService#getUploadResults
	 * (java.util.Date, java.util.Date, java.lang.String, int, int)
	 */
	@Override
	public List<ActionLog> getActionLogs(Date from, Date to,
			String configurationFileName, int position, int listSize) {

		if (listSize <= 0) {
			return new ArrayList<>();
		}

		BooleanExpression booleanExpression = getBooleanExpression(from, to,
				configurationFileName);

		Pageable pageable = new PageRequest(position, listSize, Direction.ASC,
				"actionStartTime");
		Page<ActionLog> uploadResults = repository.findAll(booleanExpression,
				pageable);

		return uploadResults.getContent();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.flexguse.images.uploadr.service.UploadResultService#countUploadResults
	 * (java.util.Date, java.util.Date, java.lang.String)
	 */
	@Override
	public long countUploadResults(Date from, Date to,
			String configurationFileName) {

		Pageable pageable = new PageRequest(0, 1);
		Page<ActionLog> uploadResults = repository
				.findAll(getBooleanExpression(from, to, configurationFileName),
						pageable);

		return uploadResults.getTotalElements();
	}

	/**
	 * This helper method creates the Query DSL boolean expression.
	 * 
	 * @param from
	 * @param to
	 * @param configurationFilename
	 * @return
	 */
	private BooleanExpression getBooleanExpression(Date from, Date to,
			String configurationFilename) {

		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -2);

		Date usedFromDate = cal.getTime();
		Date usedToDate = new Date();

		if (from != null) {
			usedFromDate = from;
		}

		if (to != null) {
			usedToDate = to;
		}

		// set from date to start of day
		cal.setTime(usedFromDate);
		cal.set(Calendar.HOUR, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		usedFromDate = cal.getTime();

		// set to date to end of day
		cal.setTime(usedToDate);
		cal.set(Calendar.HOUR, 23);
		cal.set(Calendar.MINUTE, 59);
		cal.set(Calendar.SECOND, 59);
		cal.set(Calendar.MILLISECOND, 999);
		usedToDate = cal.getTime();

		// create Query DSL boolean expression
		QActionLog actionLogQuery = QActionLog.actionLog;
		BooleanExpression booleanExpression = actionLogQuery.actionStartTime
				.between(usedFromDate, usedToDate);

		if (StringUtils.isNotBlank(configurationFilename)) {

			booleanExpression = booleanExpression
					.and(actionLogQuery.configurationFileName
							.eq(configurationFilename));
		}

		return booleanExpression;

	}

}
