package de.flexguse.images.uploadr.service;

import java.util.List;

import de.flexguse.images.uploadr.model.LockInformation;

/**
 * In the Uploadr a command can only run once at a time. This service is
 * responsible for storing the information which command currently runs with
 * which configuration.
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public interface LockInformationService {

	/**
	 * Locks the command. Normally used when command is started.
	 * 
	 * @param commandName
	 * @param configurationFile
	 */
	public void lockCommand(String commandName, String configurationFile)
			throws AlreadyLockedException;

	/**
	 * Unlocks the command. Normally used when command is stopped.
	 * 
	 * @param commandName
	 * @param configurationFile
	 */
	public void unlockCommand(String commandName, String configurationFile);

	/**
	 * Checks if the command already runs.
	 * 
	 * @param commandName
	 * @param configurationFile
	 * @return true if command is running, else false.
	 */
	public boolean isLocked(String commandName, String configurationFile);

	/**
	 * Gets a list of all available lock information in the system ordered by
	 * start date ascending.
	 * 
	 * @return
	 */
	public List<LockInformation> getAllLockInformations();

}
