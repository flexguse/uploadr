/**
 * 
 */
package de.flexguse.images.uploadr.service;

import java.io.FileFilter;

/**
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public interface UploadrService {

	/**
	 * Sets the file filter which is used to browse the filesystem
	 * 
	 * @param fileFilter
	 */
	public void setFileFilter(FileFilter filenameFilter);

	/**
	 * Sets the uploadr which is used to store the files from the filesystem in
	 * a remote system.
	 * 
	 * @param externalUploadr
	 */
	public void setExternalUploadr(ExternalUploadr externalUploadr);

	/**
	 * Sets the serice which is used to check if a file was already uploaded
	 * and which is used to store the upload information.
	 * 
	 * @param uploadResultService
	 */
	public void setActionLogService(
			ActionLogService uploadResultService);

	/**
	 * Scans the given basefolder recursively for uploadable content and does
	 * the upload.
	 * 
	 * @param configFileName
	 *            the name of the config file which was used to configure the
	 *            UploadrService. Used as key in the upload database.
	 * @param baseFolder
	 *            the base folder which is used to check for uploadable content
	 */
	public void scanAndUpload(String configFileName, String baseFolder);

}
