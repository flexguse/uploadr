/**
 * 
 */
package de.flexguse.images.uploadr.service.flickr;

import java.awt.Desktop;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.googlecode.flickrjandroid.Flickr;
import com.googlecode.flickrjandroid.FlickrException;
import com.googlecode.flickrjandroid.REST;
import com.googlecode.flickrjandroid.RequestContext;
import com.googlecode.flickrjandroid.auth.Permission;
import com.googlecode.flickrjandroid.oauth.OAuth;
import com.googlecode.flickrjandroid.oauth.OAuthToken;
import com.googlecode.flickrjandroid.photosets.Photoset;
import com.googlecode.flickrjandroid.photosets.Photosets;
import com.googlecode.flickrjandroid.uploader.UploadMetaData;
import com.googlecode.flickrjandroid.uploader.Uploader;

import de.flexguse.images.uploadr.model.UploadedFile;
import de.flexguse.images.uploadr.service.ExternalUploadr;

/**
 * This implementation uploads an image to flickr as private image. Additionally
 * it is tried to put the image in the appropriate album.
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public class FlickrUploadr implements ExternalUploadr {

	protected static final String FLICKR_SHARED_SECRET = "flickr.shared.secret";
	protected static final String FLICKR_OAUTH_VERIFIER = "flickr.oauth.verifier";
	protected static final String FLICKR_OAUTH_TOKEN = "flickr.oauth.token";
	protected static final String FLICKR_OAUTH_SECRET = "flickr.oauth.secret";
	protected static final String FLICKR_API_KEY = "flickr.api.key";
	protected static final String FLICKR_USER_ID = "flickr.user.id";

	private String apiKey;
	private String sharedSecret;
	private String oAuthToken;
	private String oAuthSecret;
	private String oAuthVerifier;
	private String flickrUserId;

	private Flickr flickr;

	private Logger logger = LoggerFactory.getLogger(getClass());

	public FlickrUploadr() {
	}

	public FlickrUploadr(String apiKey, String sharedSecret, String oAuthToken,
			String oAuthSecret, String oAuthVerifier) {
		this.apiKey = apiKey;
		this.sharedSecret = sharedSecret;
		this.oAuthToken = oAuthToken;
		this.oAuthSecret = oAuthSecret;
		this.oAuthVerifier = oAuthVerifier;
	}

	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}

	public void setSharedSecret(String sharedSecret) {
		this.sharedSecret = sharedSecret;
	}

	public void setoAuthToken(String oAuthToken) {
		this.oAuthToken = oAuthToken;
	}

	public void setoAuthSecret(String oAuthSecret) {
		this.oAuthSecret = oAuthSecret;
	}

	public void setoAuthVerifier(String oAuthVerifier) {
		this.oAuthVerifier = oAuthVerifier;
	}

	public String getApiKey() {
		return apiKey;
	}

	public String getSharedSecret() {
		return sharedSecret;
	}

	/**
	 * @param flickrUserId
	 *            the flickrUserId to set
	 */
	public void setFlickrUserId(String flickrUserId) {
		this.flickrUserId = flickrUserId;
	}

	/**
	 * Use for testing purposes only. Normally the flickr object is created in
	 * the configure method.
	 * 
	 * @param flickr
	 *            the flickr to set
	 */
	public void setFlickr(Flickr flickr) {
		this.flickr = flickr;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.flexguse.images.uploadr.service.ExternalUploadr#uploadFile(java.io
	 * .File, java.lang.String)
	 */
	@Override
	public UploadedFile uploadFile(File file, String baseDirectory) {

		if (file != null) {

			UploadedFile uploadedFile = new UploadedFile();
			uploadedFile.setUploadSuccessful(false);

			// check if file exists
			if (!file.exists()) {
				uploadedFile.setActionServiceResponse("File does not exist");
				return uploadedFile;
			}

			// check if file is readable
			if (!file.canRead()) {

				uploadedFile
						.setActionServiceResponse("no read access for file");
				return uploadedFile;
			}

			return uploadToFlickr(file, baseDirectory, uploadedFile);

		}

		return null;
	}

	/**
	 * This helper method does the upload to flickr.
	 * 
	 * @param file
	 * @param baseDirectory
	 * @param uploadResult
	 */
	private UploadedFile uploadToFlickr(File file, String baseDirectory,
			UploadedFile uploadResult) {
		// upload image
		try {

			// OAuth accessOAuth = new OAuth();
			// accessOAuth.setToken(new OAuthToken(oAuthToken, oAuthSecret));
			//
			// RequestContext requestContext =
			// RequestContext.getRequestContext();
			// requestContext.setOAuth(accessOAuth);

			Uploader uploader = flickr.getUploader();
			UploadMetaData metaData = new UploadMetaData();
			metaData.setTitle(file.getName());
			metaData.setAsync(false);
			metaData.setContentType(Flickr.CONTENTTYPE_PHOTO);
			metaData.setHidden(true);
			metaData.setPublicFlag(false);
			metaData.setSafetyLevel(Flickr.SAFETYLEVEL_SAFE);

			// upload the file to flickr
			String photoId = uploader.upload(file.getName(),
					new FileInputStream(file), metaData);
			uploadResult.setRemoteId(photoId);
			uploadResult.setUploadSuccessful(true);

			// put image into album
			String albumTitle = getAlbumTitle(file, baseDirectory);
			if (albumTitle != null) {

				try {

					// check if photoset exists
					Photosets photosets = flickr.getPhotosetsInterface()
							.getList(flickrUserId);

					String photosetId = null;

					if (photosets != null) {
						Collection<Photoset> photosetsCollection = photosets
								.getPhotosets();
						for (Photoset photoset : photosetsCollection) {
							if (albumTitle.equals(photoset.getTitle())) {
								photosetId = photoset.getId();
								break;
							}
						}
					}

					if (photosetId == null) {
						photosetId = flickr
								.getPhotosetsInterface()
								.create(albumTitle, "created by FlickrUploadr",
										photoId).getId();
					} else {
						// add image to photoset
						flickr.getPhotosetsInterface().addPhoto(photosetId,
								photoId);
					}

				} catch (Exception e) {
					uploadResult
							.setActionServiceResponse("unable to assign image to gallery: "
									+ e.getMessage());
					uploadResult.setUploadSuccessful(false);
				}
			}

		} catch (Exception e) {
			uploadResult.setActionServiceResponse(e.getMessage());
			uploadResult.setUploadSuccessful(false);
		}

		return uploadResult;
	}

	/**
	 * This method gets the album title for the image
	 * 
	 * @param file
	 * @param baseDirectory
	 * @return
	 */
	public String getAlbumTitle(File file, String baseDirectory) {

		if (file != null && baseDirectory != null) {

			String absoluteFilePath = file.getAbsolutePath();
			String relativeFilePath = StringUtils.remove(absoluteFilePath,
					baseDirectory);

			String albumTitle = StringUtils.remove(relativeFilePath,
					file.getName());

			String fileSeparator = System.getProperty("file.separator");

			albumTitle = StringUtils.removeStart(albumTitle, fileSeparator);
			albumTitle = StringUtils.removeEnd(albumTitle, fileSeparator);
			albumTitle = StringUtils.replace(albumTitle, fileSeparator, "/");

			return albumTitle;

		}

		return null;

	}

	@Override
	public Map<String, Object> doOAuthentication() {

		Map<String, Object> authenticationMap = new HashMap<>();

		Flickr flickr;
		try {
			flickr = new Flickr(getApiKey(), getSharedSecret(), new REST());

			OAuthToken oAuthToken = flickr.getOAuthInterface().getRequestToken(
					"oob");
			authenticationMap.put(FLICKR_OAUTH_TOKEN,
					oAuthToken.getOauthToken());
			authenticationMap.put(FLICKR_OAUTH_SECRET,
					oAuthToken.getOauthTokenSecret());

			URL oAuthUrl = flickr.getOAuthInterface().buildAuthenticationUrl(
					Permission.DELETE, oAuthToken);
			if (java.awt.Desktop.isDesktopSupported()) {
				Desktop desktop = Desktop.getDesktop();
				desktop.browse(oAuthUrl.toURI());
			}

		} catch (ParserConfigurationException | IOException | FlickrException
				| URISyntaxException e) {
			logger.error("unable to do authentication", e);
		}

		return authenticationMap;

	}

	@Override
	public Properties configure(Properties configurationProperties) {

		setApiKey(configurationProperties.getProperty(FLICKR_API_KEY));
		setoAuthSecret(configurationProperties.getProperty(FLICKR_OAUTH_SECRET));
		setoAuthToken(configurationProperties.getProperty(FLICKR_OAUTH_TOKEN));
		setoAuthVerifier(configurationProperties
				.getProperty(FLICKR_OAUTH_VERIFIER));
		setSharedSecret(configurationProperties
				.getProperty(FLICKR_SHARED_SECRET));
		setFlickrUserId(configurationProperties.getProperty(FLICKR_USER_ID));

		flickr = new Flickr(apiKey, sharedSecret);

		/*
		 * check if last OAuth step is necessary
		 */
		if (StringUtils.isNotBlank(oAuthVerifier)) {

			try {
				OAuth accessOAuth = flickr.getOAuthInterface().getAccessToken(
						oAuthToken, oAuthSecret, oAuthVerifier);

				// reset the oAuth tokens
				setoAuthToken(accessOAuth.getToken().getOauthToken());
				configurationProperties.setProperty(FLICKR_OAUTH_TOKEN,
						oAuthToken);

				setoAuthSecret(accessOAuth.getToken().getOauthTokenSecret());
				configurationProperties.setProperty(FLICKR_OAUTH_SECRET,
						oAuthSecret);

				configurationProperties.remove(FLICKR_OAUTH_VERIFIER);

				// set the user information
				setFlickrUserId(accessOAuth.getUser().getId());
				configurationProperties.setProperty(FLICKR_USER_ID,
						flickrUserId);

				return configurationProperties;

			} catch (IOException | FlickrException e) {

				logger.error("unable to do last Flickr OAuth step", e);

			}

		}

		return null;

	}

	@Override
	public UploadedFile deleteFile(UploadedFile uploadedFile) {

		UploadedFile deletionResult = new UploadedFile();

		Flickr flickr = new Flickr(apiKey, sharedSecret);

		RequestContext requestContext = RequestContext.getRequestContext();
		OAuth accessOAuth;
		try {

			accessOAuth = new OAuth();
			accessOAuth.setToken(new OAuthToken(oAuthToken, oAuthSecret));
			requestContext.setOAuth(accessOAuth);

			flickr.getPhotosInterface().delete(uploadedFile.getRemoteId());
			deletionResult.setUploadSuccessful(true);

		} catch (IOException | FlickrException | JSONException e) {
			logger.error("unable to delete file", e);
			deletionResult.setActionServiceResponse(e.getMessage());
			deletionResult.setUploadSuccessful(false);
		}

		return deletionResult;

	}

}
