/**
 * 
 */
package de.flexguse.images.uploadr.service.impl;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;

import de.flexguse.images.uploadr.model.LockInformation;
import de.flexguse.images.uploadr.repository.LockInformationRepository;
import de.flexguse.images.uploadr.service.AlreadyLockedException;
import de.flexguse.images.uploadr.service.LockInformationService;

/**
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public class LockInformationServiceImpl implements LockInformationService {

	private LockInformationRepository lockInformationRepository;

	@Autowired(required = false)
	public void setLockInformationRepository(
			LockInformationRepository lockInformationRepository) {
		this.lockInformationRepository = lockInformationRepository;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.flexguse.images.uploadr.service.LockInformationService#lockCommand
	 * (java.lang.String, java.lang.String)
	 */
	@Override
	public void lockCommand(String commandName, String configurationFile)
			throws AlreadyLockedException {

		if (StringUtils.isNotBlank(configurationFile)
				&& StringUtils.isNotBlank(configurationFile)) {

			// check if already locked
			LockInformation lockInformation = lockInformationRepository
					.findByCommandAndConfigurationFile(commandName,
							configurationFile);
			if (lockInformation != null) {
				// throw exception if already locked
				throw new AlreadyLockedException();
			} else {
				// save lock information
				lockInformation = new LockInformation();
				lockInformation.setCommand(commandName);
				lockInformation.setConfigurationFile(configurationFile);
				lockInformationRepository.save(lockInformation);

			}
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.flexguse.images.uploadr.service.LockInformationService#unlockCommand
	 * (java.lang.String, java.lang.String)
	 */
	@Override
	public void unlockCommand(String commandName, String configurationFile) {

		if (StringUtils.isNotBlank(commandName)
				&& StringUtils.isNotBlank(configurationFile)) {
			LockInformation lockInformation = lockInformationRepository
					.findByCommandAndConfigurationFile(commandName,
							configurationFile);
			if (lockInformation != null) {
				lockInformationRepository.delete(lockInformation);
			}
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.flexguse.images.uploadr.service.LockInformationService#isLocked(java
	 * .lang.String, java.lang.String)
	 */
	@Override
	public boolean isLocked(String commandName, String configurationFile) {

		LockInformation lockInformation = lockInformationRepository
				.findByCommandAndConfigurationFile(commandName,
						configurationFile);

		return lockInformation != null;

	}

	@Override
	public List<LockInformation> getAllLockInformations() {
		
		Sort ascendingSort = new Sort("startTime");
		return lockInformationRepository.findAll(ascendingSort);
	}

}
