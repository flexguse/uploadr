/**
 * 
 */
package de.flexguse.images.uploadr.service;

import java.util.Date;
import java.util.List;

import de.flexguse.images.uploadr.model.ActionLog;

/**
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public interface ActionLogService {

	/**
	 * Checks if an UploadResults exist for the given keys.
	 * 
	 * @param configurationFilename
	 * @param fileName
	 * @param filePath
	 * @return matching {@link ActionLog} or null
	 */
	ActionLog findUploadResult(String configurationFilename,
			String fileName, String filePath);

	/**
	 * Saves the given upload result.
	 * 
	 * @param uploadResult
	 */
	void save(ActionLog uploadResult);

	/**
	 * Gets {@link ActionLog}s which are matching the given criteria.
	 * 
	 * @param from
	 *            optional, if not given 2 days before current date is used
	 * @param to
	 *            optional, if not given current date is used
	 * @param configurationFileName
	 *            optional, the name of the configuration filename. if not given
	 *            UploadResults are returned for all configuration files.
	 * @param position
	 *            number of pagination page
	 * @param listSize
	 *            pagination page size
	 * @return
	 */
	List<ActionLog> getActionLogs(Date from, Date to,
			String configurationFileName, int position, int listSize);

	/**
	 * Counts how many {@link ActionLog}s exist for the given criteria.
	 * Needed for result pagination.
	 * 
	 * @param from
	 *            optional, if not given 2 days before current date is used
	 * @param to
	 *            optional, if not given current date is used
	 * @param configurationFileName
	 *            optional, the name of the configuration filename. if not given
	 *            UploadResults are returned for all configuration files.
	 * @return number of results matching the given criteria
	 */
	long countUploadResults(Date from, Date to, String configurationFileName);

}
