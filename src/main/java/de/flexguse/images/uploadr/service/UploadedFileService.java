package de.flexguse.images.uploadr.service;

import java.util.List;

import de.flexguse.images.uploadr.model.UploadedFile;

/**
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public interface UploadedFileService {

	/**
	 * This method gets an {@link UploadedFile} with matching criteria.
	 * 
	 * @param filename
	 * @param filepath
	 * @param configurationFilename
	 * @return matching {@link UploadedFile} or null
	 */
	public UploadedFile getUploadedFile(String filename, String filepath,
			String configurationFilename);

	/**
	 * Persists (creates or updates) the given file information.
	 * 
	 * @param uploadedFile
	 */
	public void saveUploadedFile(UploadedFile uploadedFile);

	/**
	 * Removes the given file information from the persistence.
	 * 
	 * @param uploadedFile
	 */
	public void removeUploadedFile(UploadedFile uploadedFile);

	/**
	 * Gets a list of already uploaded filenames for the given filepath and
	 * configurationFilename.
	 * 
	 * @param filepath
	 * @param configurationFilename
	 * @return filled or empty list
	 */
	public List<String> getUploadedFilenames(String filepath,
			String configurationFilename);

}
