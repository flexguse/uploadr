/**
 * 
 */
package de.flexguse.images.uploadr.service;

/**
 * @author Christoph Guse, info@flexguse.de
 *
 */
public class AlreadyLockedException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3091041362961801715L;

}
