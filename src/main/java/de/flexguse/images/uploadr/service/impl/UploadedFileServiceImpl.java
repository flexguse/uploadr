/**
 * 
 */
package de.flexguse.images.uploadr.service.impl;

import java.io.File;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.mysema.query.types.expr.BooleanExpression;

import de.flexguse.images.uploadr.model.QUploadedFile;
import de.flexguse.images.uploadr.model.UploadedFile;
import de.flexguse.images.uploadr.repository.UploadedFileRepository;
import de.flexguse.images.uploadr.service.UploadedFileService;

/**
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public class UploadedFileServiceImpl implements UploadedFileService {

	@Autowired(required = false)
	private UploadedFileRepository repository;

	public void setRepository(UploadedFileRepository repository) {
		this.repository = repository;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.flexguse.images.uploadr.service.UploadedFileService#getUploadedFile
	 * (java.lang.String, java.lang.String, java.lang.String)
	 */
	@Transactional(readOnly=true)
	@Override
	public UploadedFile getUploadedFile(String filename, String filepath,
			String configurationFilename) {
		
		if(filepath != null){
			filepath = new File(filepath).toURI().toString();
		}
		
		QUploadedFile qUploadedFile = QUploadedFile.uploadedFile;
		BooleanExpression filenameExpression = null;
		if (filename == null) {
			filenameExpression = qUploadedFile.filename.isNull();
		} else {
			filenameExpression = qUploadedFile.filename.eq(filename);
		}

		BooleanExpression filePathExpression = null;
		if (filepath == null) {
			filePathExpression = qUploadedFile.filePath.isNull();
		} else {
			filePathExpression = qUploadedFile.filePath.eq(filepath);
		}

		BooleanExpression configurationFilenameExpression = null;
		if (configurationFilename == null) {
			configurationFilenameExpression = qUploadedFile.isNull();
		} else {
			configurationFilenameExpression = qUploadedFile.configurationFilename
					.eq(configurationFilename);
		}

		return repository.findOne(filenameExpression.and(filePathExpression)
				.and(configurationFilenameExpression));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.flexguse.images.uploadr.service.UploadedFileService#saveUploadedFile
	 * (de.flexguse.images.uploadr.model.UploadedFile)
	 */
	@Transactional
	@Override
	public void saveUploadedFile(UploadedFile uploadedFile) {

		if (uploadedFile != null) {
			repository.save(uploadedFile);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.flexguse.images.uploadr.service.UploadedFileService#removeUploadedFile
	 * (de.flexguse.images.uploadr.model.UploadedFile)
	 */
	@Transactional
	@Override
	public void removeUploadedFile(UploadedFile uploadedFile) {

		if (uploadedFile != null) {
			repository.delete(uploadedFile);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.flexguse.images.uploadr.service.UploadedFileService#getUploadedFilenames
	 * (de.flexguse.images.uploadr.model.UploadedFile)
	 */
	@Transactional(readOnly=true)
	@Override
	public List<String> getUploadedFilenames(String filepath,
			String configurationFilename) {
		
		if(StringUtils.isNotBlank(filepath)){
			filepath = new File(filepath).toURI().toString();
		}
		
		return repository.getUploadedFilenames(filepath, configurationFilename);
	}

}
