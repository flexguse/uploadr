/**
 * 
 */
package de.flexguse.images.uploadr.service.impl;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.filefilter.DirectoryFileFilter;
import org.apache.commons.lang3.StringUtils;

import de.flexguse.images.uploadr.model.ActionLog;
import de.flexguse.images.uploadr.model.UploadedFile;
import de.flexguse.images.uploadr.service.ExternalUploadr;
import de.flexguse.images.uploadr.service.ActionLogService;
import de.flexguse.images.uploadr.service.UploadedFileService;
import de.flexguse.images.uploadr.service.UploadrService;

/**
 * This implementation does the upload using the externalUploadr.
 * <p>
 * Ensure this implementation is not used as singleton but use a new instance
 * for each time.
 * </p>
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public class UploadrServiceImpl implements UploadrService {

	private FileFilter fileFilter = new JPEGFileFilter();
	private ExternalUploadr externalUploadr;
	private ActionLogService actionLogService;
	private UploadedFileService uploadedFileService;

	private String baseFolder;
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.flexguse.images.uploadr.service.UploadrService#setFilenameFilter(java
	 * .io.FilenameFilter)
	 */
	@Override
	public void setFileFilter(FileFilter fileFilter) {
		this.fileFilter = fileFilter;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.flexguse.images.uploadr.service.UploadrService#setExternalUploadr(
	 * de.flexguse.images.uploadr.service.ExternalUploadr)
	 */
	@Override
	public void setExternalUploadr(ExternalUploadr externalUploadr) {
		this.externalUploadr = externalUploadr;
	}

	@Override
	public void setActionLogService(ActionLogService actionLogService) {
		this.actionLogService = actionLogService;
	}

	public void setUploadedFileService(UploadedFileService uploadedFileService) {
		this.uploadedFileService = uploadedFileService;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.flexguse.images.uploadr.service.UploadrService#scanAndUpload(java.
	 * lang.String, java.lang.String)
	 */
	@Override
	public void scanAndUpload(String configFileName, String baseFolder) {
		this.baseFolder = baseFolder;
		walkFolderStructure(configFileName, baseFolder);
	}

	/**
	 * This helper method walks through the given folder, checks for files
	 * matching the given filename filter and uses the external uploadr to
	 * upload the files.
	 * 
	 * @param configFileName
	 * @param folder
	 */
	private void walkFolderStructure(String configFileName, String folder) {

		if (StringUtils.isNotBlank(folder)) {

			File searchedFolder = new File(folder);
			if (searchedFolder.exists() && searchedFolder.canRead()) {

				// create list which contains the already uploaded filenames
				List<String> alreadyUploadedFilesInFolder = uploadedFileService
						.getUploadedFilenames(folder, configFileName);

				// get all jpg files from folder
				File[] containedFiles = searchedFolder.listFiles(fileFilter);
				if (containedFiles != null) {

					for (File file : containedFiles) {

						// only upload if file is not already uploaded
						if (file.isFile()) {

							// MD5 hash is needed in every case
							String md5 = createFileMd5Hash(file);

							// check if file was already uploaded
							if (alreadyUploadedFilesInFolder.contains(file
									.getName())) {

								// check if file has changed
								if (hasMD5Changed(md5, file, folder,
										configFileName)) {
									// delete existing remote file
									deleteExistingRemoteFile(file, folder,
											configFileName);

									// upload again
									uploadFile(file, md5, folder,
											configFileName);
								}

								// remove filename from uploaded list so it is
								// not deleted later on
								alreadyUploadedFilesInFolder.remove(file
										.getName());

							} else {
								// upload file
								uploadFile(file, md5, folder, configFileName);
							}

						}

					}

				}

				// delete remote images which were deleted locally
				for (String filename : alreadyUploadedFilesInFolder) {
					deleteExistingRemoteFile(filename, folder, configFileName);
				}

				// get all folders from folder
				String[] folders = searchedFolder
						.list(DirectoryFileFilter.DIRECTORY);
				if (folders != null) {
					for (String subFolder : folders) {
						
						StringBuilder pathBuilder = new StringBuilder(folder);
						pathBuilder.append(System.getProperty("file.separator")).append(subFolder);
						
						walkFolderStructure(configFileName, pathBuilder.toString());
					}
				}

			}

		}
	}

	/**
	 * This helper methdo creates a MD5 hash value for the given file.
	 * 
	 * @param file
	 * @return
	 */
	private String createFileMd5Hash(File file) {
		FileInputStream fis = null;
		String md5 = null;
		try {
			fis = new FileInputStream(file);
			md5 = DigestUtils.md5Hex(fis);
		} catch (IOException e) {
			// intended to do nothing, this case must not
			// appear.
		} finally {
			try {
				if (fis != null) {
					fis.close();
				}
			} catch (IOException e) {
				// intended to do nothing
			}
		}
		return md5;
	}

	/**
	 * This helper method checks if the MD5 hash value changed since putting
	 * {@link UploadedFile} into persistence.
	 * 
	 * @param currentMD5Hash
	 * @param file
	 * @param filePath
	 * @param configurationFileName
	 * @return true if the current file was changed, else false
	 */
	private boolean hasMD5Changed(String currentMD5Hash, File file,
			String filePath, String configurationFileName) {

		UploadedFile uploadedFile = uploadedFileService.getUploadedFile(
				file.getName(), filePath, configurationFileName);
		if (uploadedFile != null) {
			return !currentMD5Hash.equals(uploadedFile.getMd5hash());
		}

		return false;
	}

	/**
	 * This helper method deletes the remote file.
	 * 
	 * @param file
	 * @param filePath
	 * @param configurationFilename
	 */
	private void deleteExistingRemoteFile(File file, String filePath,
			String configurationFilename) {

		deleteExistingRemoteFile(file.getName(), filePath,
				configurationFilename);

	}

	/**
	 * This helper method deletes the remote file.
	 * 
	 * @param filename
	 * @param filePath
	 * @param configurationFilename
	 */
	private void deleteExistingRemoteFile(String filename, String filePath,
			String configurationFilename) {

		UploadedFile uploadedFile = uploadedFileService.getUploadedFile(
				filename, filePath, configurationFilename);
		if (uploadedFile != null) {
			
			ActionLog actionLog = new ActionLog();
			actionLog.setAction("delete");
			long start = System.currentTimeMillis();
			UploadedFile removedFile = externalUploadr.deleteFile(uploadedFile);
			removedFile.setFilename(filename);
			removedFile.setFilePath(filePath);
			removedFile.setConfigurationFilename(configurationFilename);
			actionLog.setActionDurationInMilliseconds(System.currentTimeMillis() - start);
			actionLog.fillFromUploadedFile(removedFile);
			actionLogService.save(actionLog);
			
			if(actionLog.isActionSuccessful()){
				uploadedFileService.removeUploadedFile(uploadedFile);
			}
			
		}

	}

	/**
	 * This helper method uploads the given file and persists the UploadResult.
	 * If the upload was successful, a UploadedFile is persisted also.
	 * 
	 * @param file
	 * @param md5hash
	 * @param filePath
	 * @param configurationFilename
	 */
	private void uploadFile(File file, String md5hash, String filePath,
			String configurationFilename) {
		
		ActionLog actionLog = new ActionLog();
		actionLog.setAction("upload");
		long start = System.currentTimeMillis();
		UploadedFile uploadedFile = externalUploadr.uploadFile(file, baseFolder);
		actionLog.setActionDurationInMilliseconds(System.currentTimeMillis() - start);
		uploadedFile.setFilename(file.getName());
		uploadedFile.setFilePath(filePath);
		uploadedFile.setConfigurationFilename(configurationFilename);
		actionLog.fillFromUploadedFile(uploadedFile);
		actionLogService.save(actionLog);
		
		if (actionLog.isActionSuccessful()) {
			uploadedFile.setMd5hash(md5hash);
			uploadedFileService.saveUploadedFile(uploadedFile);
		}

	}

}
