/**
 * 
 */
package de.flexguse.images.uploadr.service;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import de.flexguse.images.uploadr.command.UploadrCommand;
import de.flexguse.images.uploadr.command.UploadrCommandException;

/**
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public class Uploadr {

	private static ApplicationContext springContext;

	private static void startSpringContext() {
		springContext = new ClassPathXmlApplicationContext(
				"uploadr-context.xml");
	}

	public static void main(String[] args) {

		startSpringContext();

		if (args == null || args.length < 1 || StringUtils.isBlank(args[0])) {
			System.out
					.println("Please give command for the uploadr. Use 'uploadr help' for more information.");
		} else {

			String command = args[0];

			try {
				UploadrCommand uploadrCommand = (UploadrCommand) springContext
						.getBean(command);
				uploadrCommand.setUploadrArguments(args);
				uploadrCommand.execute();
			} catch (UploadrCommandException e) {
				System.out.println("uploadr terminated unexpectedly: "
						+ e.getMessage());
			} catch(NoSuchBeanDefinitionException e){
				System.out.println("The command '" + command + "' is not known. Use 'uploadr help' for more information.");
			}

		}

	}

}
