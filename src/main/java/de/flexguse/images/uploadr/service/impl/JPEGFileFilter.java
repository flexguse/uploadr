/**
 * 
 */
package de.flexguse.images.uploadr.service.impl;

import java.io.File;
import java.io.FileFilter;

import org.apache.commons.lang3.StringUtils;

/**
 * This FilenameFilter only accepts files with .jpg ending or folders. It is
 * case insensitive.
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public class JPEGFileFilter implements FileFilter {

	private static final String acceptedFileEnding = ".jpg";

	@Override
	public boolean accept(File file) {

		if (file != null) {
			if (file.isFile()) {
				return StringUtils.endsWithIgnoreCase(file.getName(),
						acceptedFileEnding);
			} 

		}
		return false;
	}

}
