/**
 * 
 */
package de.flexguse.images.uploadr.service;

import java.io.File;
import java.util.Map;
import java.util.Properties;

import de.flexguse.images.uploadr.model.UploadedFile;

/**
 * Interface for all uploaders.
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public interface ExternalUploadr {

	/**
	 * Uploads the given file to the remote location. It is up to the
	 * implementation if the given file is just uploaded or if it is persisted
	 * in some kind of folder structure.
	 * 
	 * @param file
	 *            the file to upload
	 * @param baseDirectory
	 *            the root directory from which the files are taken recursively.
	 *            Needed to calculate folder names.
	 * @return the UploadResult
	 */
	public UploadedFile uploadFile(File file, String baseDirectory);

	/**
	 * Deletes the remote file.
	 * 
	 * @param uploadedFile
	 * @return TODO
	 */
	public UploadedFile deleteFile(UploadedFile uploadedFile);

	/**
	 * Uses OAuth to authenticate the Uploadr application as user for the
	 * external service.
	 * 
	 * @return the properties containing the relevant authentication settings
	 */
	public Map<String, Object> doOAuthentication();

	/**
	 * Does the configuration (if needed).
	 * 
	 * @param properties
	 */
	public Properties configure(Properties properties);

}
