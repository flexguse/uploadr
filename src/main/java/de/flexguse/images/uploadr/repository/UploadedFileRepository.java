/**
 * 
 */
package de.flexguse.images.uploadr.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import de.flexguse.images.uploadr.model.UploadedFile;

/**
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public interface UploadedFileRepository extends
		JpaRepository<UploadedFile, Long>,
		QueryDslPredicateExecutor<UploadedFile> {

	/**
	 * Gets all filenames uploaded in the current folder (filepath) for the
	 * given configurationFilename.
	 * 
	 * @param filepath
	 * @param configurationFilename
	 * @return empty or filed list
	 */
	@Query("SELECT u.filename FROM UploadedFile u WHERE u.filePath=?1 and u.configurationFilename=?2")
	public List<String> getUploadedFilenames(String filepath,
			String configurationFilename);

}
