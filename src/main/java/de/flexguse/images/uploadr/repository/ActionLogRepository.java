package de.flexguse.images.uploadr.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import de.flexguse.images.uploadr.model.ActionLog;

/**
 * This repository is used to store and find image upload logs.
 * 
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public interface ActionLogRepository extends
		JpaRepository<ActionLog, Long>,
		QueryDslPredicateExecutor<ActionLog> {

	/**
	 * This method gets the upload result for configurationFilename, filename
	 * and filepath. The combination of the keys should be unique.
	 * <p>
	 * Use this method to check if an image was already uploaded.
	 * </p>
	 * 
	 * @param configurationFilename
	 * @param filename
	 * @param folderName
	 * @return null or matching UploadResult.
	 */
	ActionLog findOneByConfigurationFileNameAndFilenameAndFolderName(
			String configurationFilename, String filename, String folderName);

}
