/**
 * 
 */
package de.flexguse.images.uploadr.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import de.flexguse.images.uploadr.model.LockInformation;

/**
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public interface LockInformationRepository extends
		JpaRepository<LockInformation, Long> {

	/**
	 * Gets a LockInformation by command and configurationFile
	 * 
	 * @param command
	 * @param configurationFile
	 * @return null if no LockInformation exists, else {@link LockInformation}
	 */
	public LockInformation findByCommandAndConfigurationFile(
			String command, String configurationFile);

}
