/**
 * 
 */
package de.flexguse.uploadr.model;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

import de.flexguse.images.uploadr.model.ActionLog;

/**
 * @author Christoph Guse, info@flexguse.de
 *
 */
public class ActionLogTest {

	private ActionLog actionLog;
	
	@Before
	public void setUp(){
		actionLog = new ActionLog();
	}
	
	@Test
	public void testSetActionResult(){
		
		assertFalse(actionLog.isActionSuccessful());
		
		actionLog.setActionSuccessful(true);
		assertTrue(actionLog.isActionSuccessful());
		
	}
	
}
