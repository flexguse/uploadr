/**
 * 
 */
package de.flexguse.images.uploadr.model;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

/**
 * @author Christoph Guse, info@flexguse.de
 *
 */
public class UploadedFileTest {

	private UploadedFile uploadedFile;
	
	@Before
	public void setUp(){
		uploadedFile = new UploadedFile();
	}
	
	@Test
	public void testFilepathNormalization(){
		
		uploadedFile.setFilePath("C:\\some\\very\\nice\\windows\\path");
		assertEquals("file:/C:/some/very/nice/windows/path", uploadedFile.getFilePath());
		
	}
	
}
