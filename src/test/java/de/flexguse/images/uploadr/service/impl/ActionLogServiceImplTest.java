/**
 * 
 */
package de.flexguse.images.uploadr.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import de.flexguse.images.uploadr.model.ActionLog;

/**
 * @author Christoph Guse, info@flexguse.de
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:/uploadresult-service-test-context.xml")
public class ActionLogServiceImplTest {

	@Autowired
	private ActionLogServiceImpl service;

	private final String configurationFilename = "configurationFilename";
	private final String filePath = "some/file/path";

	/**
	 * save is tested implicitly
	 */
	@Rollback(true)
	@Transactional
	@Test
	public void testFindActionLog() {

		assertNull(service.findUploadResult(null, null, null));

		final String configurationFilename = "configurationFilename";
		final String fileName = "fileName";
		final String filePath = "some/file/path";

		ActionLog uploadResult = createActionLog(configurationFilename,
				fileName, filePath);
		service.save(uploadResult);

		assertEquals(uploadResult, service.findUploadResult(
				configurationFilename, fileName, filePath));
		assertNull(service.findUploadResult(null, fileName, filePath));
		assertNull(service.findUploadResult(configurationFilename, null,
				filePath));
		assertNull(service.findUploadResult(configurationFilename, fileName,
				null));

	}

	/**
	 * This helper method creates an {@link ActionLog} object.
	 * 
	 * @param configurationFilename
	 * @param fileName
	 * @param filePath
	 * @return
	 */
	private ActionLog createActionLog(final String configurationFilename,
			final String fileName, final String filePath) {
		ActionLog uploadResult = new ActionLog();
		uploadResult.setConfigurationFileName(configurationFilename);
		uploadResult.setFilename(fileName);
		uploadResult.setFolderName(filePath);
		return uploadResult;
	}

	/**
	 * This helper method creates 5 upload results in the persistence.
	 */
	private void createActionLogs() {

		String fileName1 = "fileName";
		Calendar cal = Calendar.getInstance();
		ActionLog uploadResult1 = createActionLog(configurationFilename,
				fileName1, filePath);
		uploadResult1.setActionStartTime(cal.getTime());
		service.save(uploadResult1);

		String fileName2 = "fileName2";
		ActionLog uploadResult2 = createActionLog(configurationFilename,
				fileName2, filePath);
		cal.add(Calendar.DATE, -1);
		uploadResult2.setActionStartTime(cal.getTime());
		service.save(uploadResult2);

		String fileName3 = "fileName3";
		ActionLog uploadResult3 = createActionLog(configurationFilename,
				fileName3, filePath);
		cal.add(Calendar.DATE, -1);
		uploadResult3.setActionStartTime(cal.getTime());
		service.save(uploadResult3);

		String fileName4 = "fileName4";
		ActionLog uploadResult4 = createActionLog(configurationFilename,
				fileName4, filePath);
		cal.add(Calendar.DATE, -1);
		uploadResult4.setActionStartTime(cal.getTime());
		service.save(uploadResult4);

		String fileName5 = "fileName5";
		ActionLog uploadResult5 = createActionLog(
				"otherConfigurationFilename", fileName5, filePath);
		cal.add(Calendar.DATE, -1);
		uploadResult5.setActionStartTime(cal.getTime());
		service.save(uploadResult5);
	}

	@Rollback(true)
	@Transactional
	@Test
	public void testCountActionLogs() {

		assertEquals(0, service.countUploadResults(null, null, null));

		createActionLogs();

		assertEquals("only 3 UploadResults expected for the default settings",
				3,
				service.countUploadResults(null, null, configurationFilename));

		Calendar fromCalendar = Calendar.getInstance();
		fromCalendar.add(Calendar.DATE, -4);

		assertEquals(4, service.countUploadResults(fromCalendar.getTime(),
				new Date(), configurationFilename));

		fromCalendar.add(Calendar.DATE, -1);
		assertEquals(5, service.countUploadResults(fromCalendar.getTime(),
				new Date(), null));

	}

	@Rollback(true)
	@Transactional
	@Test
	public void testGetActionLogs() {

		List<ActionLog> actionLogs = service.getActionLogs(null, null,
				null, 0, 0);
		assertEquals(0, actionLogs.size());
		
		createActionLogs();
		
		actionLogs = service.getActionLogs(null, null, configurationFilename, 0, 1);
		
		ActionLog check = actionLogs.get(0);
		assertEquals("upload result 3 expected because of default from-date and ASC sort by start time", "fileName3", check.getFilename());
		
		actionLogs = service.getActionLogs(null, null, configurationFilename, 1, 1);
		check = actionLogs.get(0);
		assertEquals("fileName2", check.getFilename());

	}

}
