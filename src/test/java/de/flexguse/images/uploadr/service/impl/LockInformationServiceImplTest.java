/**
 * 
 */
package de.flexguse.images.uploadr.service.impl;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import de.flexguse.images.uploadr.model.LockInformation;
import de.flexguse.images.uploadr.repository.LockInformationRepository;
import de.flexguse.images.uploadr.service.AlreadyLockedException;

/**
 * @author Christoph Guse, info@flexguse.de
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:/lockinformation-service-test-context.xml")
public class LockInformationServiceImplTest {

	@Autowired
	private LockInformationServiceImpl service;
	
	@Autowired
	private LockInformationRepository repository;

	@Transactional
	@Rollback(true)
	@Test
	public void testLock() throws AlreadyLockedException {

		// must not throw exception
		service.lockCommand(null, null);
		service.lockCommand("command", "");
		service.lockCommand("", "configurationFile");
		
		String command = "some very nice command";
		String configurationFile = "/some/path/to/the/configuration/file.properties";
		service.lockCommand(command, configurationFile);
		assertTrue(service.isLocked(command, configurationFile));
		
	}
	
	@Transactional
	@Rollback(true)
	@Test(expected=AlreadyLockedException.class)
	public void testLockException() throws AlreadyLockedException{
				
		String command = "some very nice command";
		String configurationFile = "/some/path/to/the/configuration/file.properties";
		service.lockCommand(command, configurationFile);
		service.lockCommand(command, configurationFile);
		
	}
	
	@Transactional
	@Rollback(true)
	@Test
	public void testUnLock() throws AlreadyLockedException {

		// must not throw exception
		service.unlockCommand(null, null);
		service.unlockCommand("command", "");
		service.unlockCommand("", "configurationFile");
		
		String command = "some very nice command";
		String configurationFile = "/some/path/to/the/configuration/file.properties";
		service.unlockCommand(command, configurationFile);
		
		service.lockCommand(command, configurationFile);
		assertTrue(service.isLocked(command, configurationFile));
		
		service.unlockCommand(command, configurationFile);
		assertFalse(service.isLocked(command, configurationFile));
		
	}
	
	@Transactional
	@Rollback(true)
	@Test
	public void testGetAllLockInfos(){
		
		assertEquals(0, service.getAllLockInformations().size());
		
		LockInformation li1 = new LockInformation();
		LockInformation li2 = new LockInformation();
		repository.save(li1);
		repository.save(li2);
		
		List<LockInformation> informations = service.getAllLockInformations();
		assertEquals("2 lock informations expected", 2, informations.size());
		assertEquals(li1, informations.get(0));
		assertEquals(li2, informations.get(1));
		
		
	}

}
