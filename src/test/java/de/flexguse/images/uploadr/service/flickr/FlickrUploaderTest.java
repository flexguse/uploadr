/**
 * 
 */
package de.flexguse.images.uploadr.service.flickr;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.Arrays;
import java.util.Properties;

import javax.xml.parsers.ParserConfigurationException;

import org.json.JSONException;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.xml.sax.SAXException;

import com.googlecode.flickrjandroid.Flickr;
import com.googlecode.flickrjandroid.FlickrException;
import com.googlecode.flickrjandroid.REST;
import com.googlecode.flickrjandroid.auth.Permission;
import com.googlecode.flickrjandroid.oauth.OAuthToken;
import com.googlecode.flickrjandroid.photosets.Photoset;
import com.googlecode.flickrjandroid.photosets.Photosets;
import com.googlecode.flickrjandroid.photosets.PhotosetsInterface;
import com.googlecode.flickrjandroid.uploader.UploadMetaData;
import com.googlecode.flickrjandroid.uploader.Uploader;

import de.flexguse.images.uploadr.model.UploadedFile;

/**
 * @author Christoph Guse, info@flexguse.de
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:/flickr-uploader-test-context.xml")
public class FlickrUploaderTest {

	private Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private FlickrUploadr uploadr;

	@Test
	public void testGetAlbumTitle() throws UnsupportedEncodingException {

		assertNull("null values must not throw any exception",
				uploadr.getAlbumTitle(null, null));
		assertNull(uploadr.getAlbumTitle(null, "/"));

		File testImageFile = new File(URLDecoder.decode(
				getClass().getResource("/images/test1/test-image-1.jpg")
						.getFile(), "UTF-8"));
		assertNotNull("test image file expected", testImageFile);

		assertEquals("test1", uploadr.getAlbumTitle(testImageFile,
				testImageFile.getParentFile().getParent()));

		assertEquals(
				"images/test1",
				uploadr.getAlbumTitle(testImageFile, testImageFile
						.getParentFile().getParentFile().getParent()));

	}

	/**
	 * Integration test which should not be executed in the normal build
	 * process.
	 * 
	 * This test can only run once because the {@link FlickrUploadr} changes the
	 * content of the properties files which is in this test not written to file
	 * system. The properties update is done by the UploadCommand only.
	 * 
	 * @throws IOException
	 */
	@Ignore
	@Test
	public void testUpload() throws IOException {

		File testImageFile = new File(URLDecoder.decode(
				getClass().getResource("/images/test2/test-image-2.jpg")
						.getFile(), "UTF-8"));

		Properties configurationProperties = new Properties();
		configurationProperties.load(getClass().getResourceAsStream(
				"/configuration/flickr.properties"));
		uploadr.configure(configurationProperties);

		UploadedFile uploadResult = uploadr.uploadFile(testImageFile,
				testImageFile.getParentFile().getParent());
		assertTrue("successful upload expected",
				uploadResult.isUploadSuccessful());

	}

	/**
	 * Only needed to get the OAuth Token, OAuth Secret and OAuth verification.
	 * The verification is given by setting the 'oob' parameter as return URL.
	 * <p>
	 * Opens a browser window to grant access, OAuth Token and OAuth secret must
	 * be taken from console, OAuth verification from flickr site.
	 * </p>
	 * 
	 * @throws ParserConfigurationException
	 * @throws IOException
	 * @throws FlickrException
	 * @throws URISyntaxException
	 */
	@Ignore
	@Test
	public void testGetFlickrCredentials() throws ParserConfigurationException,
			IOException, FlickrException, URISyntaxException {

		Flickr flickr = new Flickr(uploadr.getApiKey(),
				uploadr.getSharedSecret(), new REST());
		OAuthToken oAuthToken = flickr.getOAuthInterface().getRequestToken(
				"oob");
		logger.error("OAuthToken: " + oAuthToken.getOauthToken());
		logger.error("OAuthSecret: " + oAuthToken.getOauthTokenSecret());

		URL oAuthUrl = flickr.getOAuthInterface().buildAuthenticationUrl(
				Permission.WRITE, oAuthToken);
		if (java.awt.Desktop.isDesktopSupported()) {
			Desktop desktop = Desktop.getDesktop();
			desktop.browse(oAuthUrl.toURI());
		}
	}

	@Test
	public void testNPEUploadingImage() throws IOException, FlickrException, SAXException {

		/*
		 * mock flickr behavior
		 */
		Flickr flickr = mock(Flickr.class);

		Uploader uploader = mock(Uploader.class);
		when(flickr.getUploader()).thenReturn(uploader);

		when(
				uploader.upload(anyString(), (InputStream) anyObject(),
						(UploadMetaData) anyObject()))
				.thenThrow(
						new NullPointerException(
								"something went wrong with the stream"));
		
		uploadr.setFlickr(flickr);
		
		
		/*
		 * test upload
		 */
		File testImageFile = new File(URLDecoder.decode(
				getClass().getResource("/images/test2/test-image-2.jpg")
						.getFile(), "UTF-8"));
		
		// must not throw an exception
		UploadedFile uploadResult = uploadr.uploadFile(testImageFile, null);
		assertFalse("upload must be marked as insuccessful", uploadResult.isUploadSuccessful());

	}
	
	@Test
	public void testExceptionWhileGettingPhotosetsList() throws FlickrException, IOException, SAXException, JSONException{
		
		/*
		 * mock flickr behavior
		 */
		Flickr flickr =  mock(Flickr.class);
		uploadr.setFlickr(flickr);
		
		Uploader uploader = mock(Uploader.class);
		when(flickr.getUploader()).thenReturn(uploader);
		
		PhotosetsInterface psInterface = mock(PhotosetsInterface.class);
		when(psInterface.getList(anyString())).thenThrow(new NullPointerException("Error getting photosets list"));
		
		when(uploader.upload(anyString(), (InputStream)anyObject(), (UploadMetaData)anyObject())).thenReturn("uploadedNo1");
		
		/*
		 * test upload
		 */
		File testImageFile = new File(URLDecoder.decode(
				getClass().getResource("/images/test2/test-image-2.jpg")
						.getFile(), "UTF-8"));
		
		// must not throw an exception
		UploadedFile uploadResult = uploadr.uploadFile(testImageFile, "/some/base/directory");
		assertFalse("upload must be marked as insuccessful", uploadResult.isUploadSuccessful());
		assertEquals("uploadedNo1", uploadResult.getRemoteId());
		
	}
	
	@Test
	public void testExceptionCreatingAlbum() throws IOException, FlickrException, JSONException, SAXException{
		
		/*
		 * mock flickr behavior
		 */
		Flickr flickr =  mock(Flickr.class);
		uploadr.setFlickr(flickr);
		
		Uploader uploader = mock(Uploader.class);
		when(flickr.getUploader()).thenReturn(uploader);
		
		PhotosetsInterface psInterface = mock(PhotosetsInterface.class);
		when(flickr.getPhotosetsInterface()).thenReturn(psInterface);
		
		Photosets photosets = mock(Photosets.class);
		when(psInterface.getList(anyString())).thenReturn(photosets);
		
		when(psInterface.create(anyString(), anyString(), anyString())).thenThrow(new NullPointerException("unable to create Photoset"));
		
		when(uploader.upload(anyString(), (InputStream)anyObject(), (UploadMetaData)anyObject())).thenReturn("uploadedNo1");
		
		
		/*
		 * test upload
		 */
		File testImageFile = new File(URLDecoder.decode(
				getClass().getResource("/images/test2/test-image-2.jpg")
						.getFile(), "UTF-8"));
		
		// must not throw an exception
		UploadedFile uploadResult = uploadr.uploadFile(testImageFile, "/some/base/directory");
		assertFalse("upload must be marked as insuccessful", uploadResult.isUploadSuccessful());
		assertEquals("uploadedNo1", uploadResult.getRemoteId());
		
	}
	
	@Test
	public void testAddPhotoException() throws IOException, FlickrException, JSONException, SAXException{
	
		/*
		 * mock flickr behavior
		 */
		Flickr flickr =  mock(Flickr.class);
		uploadr.setFlickr(flickr);
		
		Uploader uploader = mock(Uploader.class);
		when(flickr.getUploader()).thenReturn(uploader);
		
		when(uploader.upload(anyString(), (InputStream)anyObject(), (UploadMetaData)anyObject())).thenReturn("uploadedNo1");
		
		PhotosetsInterface psInterface = mock(PhotosetsInterface.class);
		when(flickr.getPhotosetsInterface()).thenReturn(psInterface);
		
		Photosets photosets = mock(Photosets.class);
		when(psInterface.getList(anyString())).thenReturn(photosets);
				
		Photoset photoSet = mock(Photoset.class);
		when(photosets.getPhotosets()).thenReturn(Arrays.asList(photoSet));
		
		doThrow(new NullPointerException("error adding image to album")).when(psInterface).addPhoto(anyString(), anyString());
				
		
		/*
		 * test upload
		 */
		File testImageFile = new File(URLDecoder.decode(
				getClass().getResource("/images/test2/test-image-2.jpg")
						.getFile(), "UTF-8"));
		
		when(photoSet.getTitle()).thenReturn(testImageFile.getAbsolutePath());
		
		// must not throw an exception
		UploadedFile uploadResult = uploadr.uploadFile(testImageFile, "/some/base/directory");
		assertFalse("upload must be marked as insuccessful", uploadResult.isUploadSuccessful());
		assertEquals("uploadedNo1", uploadResult.getRemoteId());
		
	}

}
