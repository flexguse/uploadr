/**
 * 
 */
package de.flexguse.images.uploadr.service.impl;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.FileFilter;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import org.junit.Test;

/**
 * @author Christoph Guse, info@flexguse.de
 *
 */
public class JPEGFileFilterTest {

	private FileFilter jpegFilenameFilter = new JPEGFileFilter();
	
	@Test
	public void testFilter() throws UnsupportedEncodingException{
		
		// get the images/test1 folder
		String filepath = URLDecoder.decode(getClass().getResource("/images/test1/test-image-1.jpg").getFile(), "UTF-8");
		File imageFile = new File(filepath);
		File imageFolder = imageFile.getParentFile();
		
		File[] files = imageFolder.listFiles(jpegFilenameFilter);
		assertEquals("2 JPG files expected", 2, files.length);
		
	}
	
}
