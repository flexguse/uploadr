/**
 * 
 */
package de.flexguse.images.uploadr.service.impl;

import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import de.flexguse.images.uploadr.model.ActionLog;
import de.flexguse.images.uploadr.model.UploadedFile;
import de.flexguse.images.uploadr.service.ActionLogService;
import de.flexguse.images.uploadr.service.ExternalUploadr;
import de.flexguse.images.uploadr.service.UploadedFileService;

/**
 * @author Christoph Guse, info@flexguse.de
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:/uploadr-service-impl-test-context.xml")
public class UploadrServiceImplTest {

	@Autowired
	private UploadrServiceImpl uploadrService;

	@Test
	public void testSuccessfulUploadImage()
			throws UnsupportedEncodingException, URISyntaxException {

		String configFileName = "flickr.conf";

		// set mocked objects in uploadrService
		uploadrService.setFileFilter(new JPEGFileFilter());

		ExternalUploadr mockedExternalUploadr = mock(ExternalUploadr.class);
		UploadedFile uploadedFile = new UploadedFile();
		uploadedFile.setUploadSuccessful(true);
		when(mockedExternalUploadr.uploadFile((File) anyObject(), anyString()))
				.thenReturn(uploadedFile);
		uploadrService.setExternalUploadr(mockedExternalUploadr);

		ActionLogService uploadResultService = mock(ActionLogService.class);
		uploadrService.setActionLogService(uploadResultService);

		// set base directory and configuration filename
		File imagesDirectory = Paths.get(
				getClass().getResource("/images").toURI()).toFile();

		// mock behavior of uploadr
		UploadedFileService mockedUploadedFileService = mock(UploadedFileService.class);
		when(
				mockedUploadedFileService.getUploadedFilenames(
						eq(imagesDirectory.getAbsolutePath()
								+ System.getProperty("file.separator")
								+ "test1"), anyString())).thenReturn(
				new ArrayList<String>(Arrays.asList("test-image-1.jpg",
						"test-image-2.JPG")));
		uploadrService.setUploadedFileService(mockedUploadedFileService);

		uploadrService.scanAndUpload(configFileName,
				imagesDirectory.getAbsolutePath());

		// only the file test2/test-image-2.jpg expected to be uploaded
		verify(mockedExternalUploadr, times(1)).uploadFile((File) anyObject(),
				anyString());
		verify(uploadResultService, times(1)).save((ActionLog) anyObject());

		// file must be marked as uploaded because external upload was
		// unsuccessful
		verify(mockedUploadedFileService, times(1)).saveUploadedFile(
				(UploadedFile) anyObject());
	}

	@Test
	public void testUnsuccessfulImageUpload() throws URISyntaxException {

		String configFileName = "flickr.conf";

		// set mocked objects in uploadrService
		uploadrService.setFileFilter(new JPEGFileFilter());

		ExternalUploadr mockedExternalUploadr = mock(ExternalUploadr.class);
		UploadedFile uploadedFile = new UploadedFile();
		uploadedFile.setUploadSuccessful(false);
		when(mockedExternalUploadr.uploadFile((File) anyObject(), anyString()))
				.thenReturn(uploadedFile);
		uploadrService.setExternalUploadr(mockedExternalUploadr);

		ActionLogService uploadResultService = mock(ActionLogService.class);
		uploadrService.setActionLogService(uploadResultService);

		// mock behavior of uploadr
		UploadedFileService mockedUploadedFileService = mock(UploadedFileService.class);
		when(
				mockedUploadedFileService.getUploadedFilenames(anyString(),
						anyString())).thenReturn(
				new ArrayList<String>(Arrays.asList("test-image-1.jpg",
						"test-image-2.JPG")));
		uploadrService.setUploadedFileService(mockedUploadedFileService);

		// set base directory and configuration filename
		File imagesDirectory = Paths.get(
				getClass().getResource("/images").toURI()).toFile();

		uploadrService.scanAndUpload(configFileName,
				imagesDirectory.getAbsolutePath());

		// only the file test2/test-image-2.jpg expected to be uploaded
		verify(mockedExternalUploadr, times(1)).uploadFile((File) anyObject(),
				anyString());
		verify(uploadResultService, times(1)).save((ActionLog) anyObject());

		// file must not be marked as uploaded because external upload was
		// unsuccessful
		verify(mockedUploadedFileService, times(0)).saveUploadedFile(
				(UploadedFile) anyObject());

	}

	/*
	 * Test updating an image because of changed hashvalue.
	 */
	@Test
	public void testUpdateImage() throws URISyntaxException {

		String configFileName = "flickr.conf";

		// set mocked objects in uploadrService
		uploadrService.setFileFilter(new JPEGFileFilter());

		ExternalUploadr mockedExternalUploadr = mock(ExternalUploadr.class);
		UploadedFile uploadedFile = new UploadedFile();
		uploadedFile.setUploadSuccessful(true);
		when(mockedExternalUploadr.uploadFile((File) anyObject(), anyString()))
				.thenReturn(uploadedFile);
				
		uploadrService.setExternalUploadr(mockedExternalUploadr);

		ActionLogService mockedActionLogService = mock(ActionLogService.class);
		uploadrService.setActionLogService(mockedActionLogService);

		// mock behavior of uploadr
		UploadedFileService mockedUploadedFileService = mock(UploadedFileService.class);

		when(
				mockedUploadedFileService.getUploadedFilenames(anyString(),
						anyString())).thenReturn(
				new ArrayList<String>(Arrays.asList("test-image-1.jpg",
						"test-image-2.JPG")));
		UploadedFile mockedTestImage1Uploaded = mock(UploadedFile.class);
		when(mockedTestImage1Uploaded.getMd5hash()).thenReturn("Hello World");
		when(
				mockedUploadedFileService
						.getUploadedFile(eq("test-image-1.jpg"), anyString(),
								eq(configFileName))).thenReturn(
				mockedTestImage1Uploaded);
		
		UploadedFile mockDeletionSuccess = mock(UploadedFile.class);
		when(mockDeletionSuccess.isUploadSuccessful()).thenReturn(true);
		when(mockedExternalUploadr.deleteFile(mockedTestImage1Uploaded)).thenReturn(mockDeletionSuccess);

		uploadrService.setUploadedFileService(mockedUploadedFileService);

		// set base directory and configuration filename
		File imagesDirectory = Paths.get(
				getClass().getResource("/images/test1").toURI()).toFile();

		uploadrService.scanAndUpload(configFileName,
				imagesDirectory.getAbsolutePath());

		verify(mockedExternalUploadr, times(1)).deleteFile(
				(UploadedFile) anyObject());
		verify(mockedUploadedFileService, times(1)).removeUploadedFile(
				(UploadedFile) anyObject());
		verify(mockedExternalUploadr, times(1)).uploadFile((File) anyObject(),
				anyString());
		verify(mockedUploadedFileService, times(1)).removeUploadedFile(
				(UploadedFile) anyObject());
		// expected to be called 2 times: when deleting and when uploading again
		verify(mockedActionLogService, times(2)).save((ActionLog)anyObject());
	}

}
