/**
 * 
 */
package de.flexguse.images.uploadr.service.impl;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import de.flexguse.images.uploadr.model.UploadedFile;
import de.flexguse.images.uploadr.repository.UploadedFileRepository;
import de.flexguse.images.uploadr.service.UploadedFileService;

/**
 * @author Christoph Guse, info@flexguse.de
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:/uploaded-file-service-test-context.xml")
public class UploadedFileServiceImplTest {

	@Autowired
	private UploadedFileService service;

	@Autowired
	private UploadedFileRepository repository;

	@Test
	@Transactional
	@Rollback(true)
	public void testSaveUploadedFile() {

		// must not throw an exception
		service.saveUploadedFile(null);

		service.saveUploadedFile(new UploadedFile());
		service.saveUploadedFile(new UploadedFile());

		List<UploadedFile> allFiles = repository.findAll();
		assertEquals("2 saved UploadedFiles expected", 2, allFiles.size());

	}

	@Test
	@Transactional
	@Rollback(true)
	public void testRemoveUploadedFile() {

		// must not throw an exception
		service.removeUploadedFile(null);

		UploadedFile file = new UploadedFile();
		service.saveUploadedFile(file);

		service.removeUploadedFile(file);
		assertEquals("no more uploaded file expected after deletion", 0,
				repository.findAll().size());

	}

	@Test
	@Transactional
	@Rollback(true)
	public void testGetUploadedFile() {

		// must not throw an exception
		assertNull(service.getUploadedFile(null, null, null));

		String configurationName = "some.properties";
		String filepath = "D:\\Users\\Knuffel\\Development\\common-playground\\uploadr\\target\\test-classes\\images\\test2";
		
		UploadedFile file1 = new UploadedFile();
		file1.setConfigurationFilename(configurationName);
		file1.setFilename("file1.jpg");
		file1.setFilePath(filepath);
		service.saveUploadedFile(file1);
		
		UploadedFile file2 = new UploadedFile();
		file2.setConfigurationFilename(configurationName);
		file2.setFilename("file2.jpg");
		file2.setFilePath(filepath);
		service.saveUploadedFile(file2);
		
		assertEquals(file2, service.getUploadedFile("file2.jpg", filepath, configurationName));
		assertNull(service.getUploadedFile("file2.jpg", filepath, "other.properties"));
	}
	
	@Test
	@Transactional
	@Rollback(true)
	public void testGetUploadedFilenames(){
	
		List<String> filenames = service.getUploadedFilenames(null, null);
		assertEquals("expected no filenames", 0, filenames.size());
		
		String configurationName = "some.properties";
		String filepath = "/some/file/path";
		
		UploadedFile file1 = new UploadedFile();
		file1.setConfigurationFilename(configurationName);
		file1.setFilename("file1.jpg");
		file1.setFilePath(filepath);
		service.saveUploadedFile(file1);
		
		UploadedFile file2 = new UploadedFile();
		file2.setConfigurationFilename(configurationName);
		file2.setFilename("file2.jpg");
		file2.setFilePath(filepath);
		service.saveUploadedFile(file2);
		
		filenames = service.getUploadedFilenames(filepath, configurationName);
		assertEquals(2, filenames.size());
		assertTrue(filenames.contains("file1.jpg"));
		assertTrue(filenames.contains("file2.jpg"));
		
		filenames = service.getUploadedFilenames(filepath, "other.properties");
		assertEquals(0, filenames.size());
		
	}

}
