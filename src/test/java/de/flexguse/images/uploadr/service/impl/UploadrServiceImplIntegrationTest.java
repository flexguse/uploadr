/**
 * 
 */
package de.flexguse.images.uploadr.service.impl;

import static org.mockito.Mockito.*;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Properties;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import de.flexguse.images.uploadr.model.ActionLog;
import de.flexguse.images.uploadr.model.UploadedFile;
import de.flexguse.images.uploadr.repository.UploadedFileRepository;
import de.flexguse.images.uploadr.service.ActionLogService;
import de.flexguse.images.uploadr.service.ExternalUploadr;
import de.flexguse.images.uploadr.service.flickr.FlickrUploadr;

/**
 * @author Christoph Guse, info@flexguse.de
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/repository-test-context.xml",
		"classpath:/uploadr-test-context.xml" })
public class UploadrServiceImplIntegrationTest {

	private Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private UploadrServiceImpl uploadrServiceImpl;

	@Autowired
	private UploadedFileRepository uploadedFileRepository;

	@Autowired
	private ActionLogService logService;

	@Autowired
	private FlickrUploadr flickrUploadr;

	@Before
	public void setUp() throws IOException {

		Properties flickrProperties = new Properties();
		flickrProperties.load(getClass().getResourceAsStream(
				"/configuration/flickr.properties"));
		flickrUploadr.configure(flickrProperties);

		uploadrServiceImpl.setExternalUploadr(flickrUploadr);

	}

	@Ignore
	@SuppressWarnings("unused")
	@Transactional
	@Rollback(true)
	@Test
	public void integrationTest() throws URISyntaxException {

		Path baseFolderPath = Paths.get(getClass().getResource("/images")
				.toURI());

		uploadrServiceImpl.scanAndUpload("flickr.properties",
				baseFolderPath.toString());

		uploadrServiceImpl.scanAndUpload("flickr.properties",
				baseFolderPath.toString());

		List<ActionLog> logs = logService.getActionLogs(null, null,
				"flickr.properties", 0, 1000);

		for (ActionLog log : logs) {
			logger.debug(log.isActionSuccessful() + " " + log.getFolderName()
					+ " " + log.getFilename() + " "
					+ log.getRemoteServiceResponse());
		}

		String test = "hello";

	}
	
	@Transactional
	@Rollback(true)
	@Test
	public void integrationTestWithMockedFlickrUploadr() throws URISyntaxException{
		
		final String configurationFileName = "flickr.properties";
		String actionServiceResponse = "success";
		
		Path baseFolderPath = Paths.get(getClass().getResource("/images")
				.toURI());
		
		/*
		 * mock the ExternalUploadr
		 */
		ExternalUploadr mockedExternalUploadr = mock(ExternalUploadr.class);
		uploadrServiceImpl.setExternalUploadr(mockedExternalUploadr);
		
		String baseFolder = Paths.get(getClass().getResource("/images").toURI()).toFile().getAbsolutePath();
		String test1Folder = Paths.get(getClass().getResource("/images/test1").toURI()).toFile().getAbsolutePath();
		File testImage1 = Paths.get(getClass().getResource("/images/test1/test-image-1.jpg").toURI()).toFile();
		File testImage2 = Paths.get(getClass().getResource("/images/test1/test-image-2.JPG").toURI()).toFile();
		String test2Folder = Paths.get(getClass().getResource("/images/test2").toURI()).toFile().getAbsolutePath();
		File testImage3 = Paths.get(getClass().getResource("/images/test2/test-image-2.jpg").toURI()).toFile();
		
		UploadedFile testImage1Upload = new UploadedFile();
		testImage1Upload.setActionServiceResponse(actionServiceResponse);
		testImage1Upload.setConfigurationFilename(configurationFileName);
		testImage1Upload.setFilename(testImage1.getName());
		testImage1Upload.setFilePath(test1Folder);
		testImage1Upload.setUploadSuccessful(true);
		when(mockedExternalUploadr.uploadFile(eq(testImage1), eq(baseFolder))).thenReturn(testImage1Upload);
		
		UploadedFile testImage2Upload = new UploadedFile();
		testImage2Upload.setActionServiceResponse(actionServiceResponse);
		testImage2Upload.setConfigurationFilename(configurationFileName);
		testImage2Upload.setFilename(testImage2.getName());
		testImage2Upload.setFilePath(test1Folder);
		testImage2Upload.setUploadSuccessful(true);
		when(mockedExternalUploadr.uploadFile(eq(testImage2), eq(baseFolder))).thenReturn(testImage2Upload);
		
		UploadedFile testImage3Upload = new UploadedFile();
		testImage3Upload.setActionServiceResponse(actionServiceResponse);
		testImage3Upload.setConfigurationFilename(configurationFileName);
		testImage3Upload.setFilename(testImage3.getName());
		testImage3Upload.setFilePath(test2Folder);
		testImage3Upload.setUploadSuccessful(true);
		when(mockedExternalUploadr.uploadFile(eq(testImage3), eq(baseFolder))).thenReturn(testImage3Upload);
		
		/*
		 * run scanAndUpload twice and check the number of upload executions in ExternalUploadr
		 */
		uploadrServiceImpl.scanAndUpload(configurationFileName,
				baseFolderPath.toString());
		
		uploadrServiceImpl.scanAndUpload(configurationFileName,
				baseFolderPath.toString());
		
		verify(mockedExternalUploadr, times(1)).uploadFile(eq(testImage1), eq(baseFolder));
		verify(mockedExternalUploadr, times(1)).uploadFile(eq(testImage2), eq(baseFolder));
		verify(mockedExternalUploadr, times(1)).uploadFile(eq(testImage3), eq(baseFolder));

		
	}

}
