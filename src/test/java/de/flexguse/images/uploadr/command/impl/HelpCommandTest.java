package de.flexguse.images.uploadr.command.impl;

import org.junit.Before;
import org.junit.Test;

import de.flexguse.images.uploadr.command.UploadrCommandException;
import de.flexguse.images.uploadr.command.impl.HelpCommand;

public class HelpCommandTest{

	private HelpCommand helpCommand;
	
	@Before
	public void setUp(){
		helpCommand = new HelpCommand();
	}
	
	@Test
	public void testExecute() throws UploadrCommandException{
		helpCommand.execute();
	}
	
}
