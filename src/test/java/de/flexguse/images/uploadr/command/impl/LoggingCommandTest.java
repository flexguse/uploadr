/**
 * 
 */
package de.flexguse.images.uploadr.command.impl;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import de.flexguse.images.uploadr.command.UploadrCommandException;
import de.flexguse.images.uploadr.command.impl.LoggingCommand;
import de.flexguse.images.uploadr.model.ActionLog;
import de.flexguse.images.uploadr.service.ActionLogService;

/**
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public class LoggingCommandTest {

	private LoggingCommand loggingCommand;

	@Before
	public void setUp() {
		loggingCommand = new LoggingCommand();
	}

	@Test
	public void testWithNoArguments() throws UploadrCommandException {
		
		ActionLogService service = mock(ActionLogService.class);
		
		loggingCommand.setUploadResultService(service);
		when(service.countUploadResults((Date)anyObject(), (Date)anyObject(), anyString())).thenReturn(30L);
		when(service.getActionLogs((Date)anyObject(), (Date)anyObject(), anyString(), anyInt(), anyInt())).thenReturn(actionLogs());
		loggingCommand.execute();
		
	}

	@Test
	public void testWithFromDate() throws UploadrCommandException{

		ActionLogService service = mock(ActionLogService.class);
		
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM-dd-yyyy");
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -2);
		cal.set(Calendar.HOUR, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		Date fromDate = cal.getTime();
		
		String[] uploadrArguments = {"logging", simpleDateFormat.format(fromDate)};
		loggingCommand.setUploadrArguments(uploadrArguments);
		
		loggingCommand.setUploadResultService(service);
		when(service.countUploadResults((Date)anyObject(), (Date)anyObject(), anyString())).thenReturn(30L);
		when(service.getActionLogs((Date)anyObject(), (Date)anyObject(), anyString(), anyInt(), anyInt())).thenReturn(actionLogs());
		loggingCommand.execute();
		verify(service, times(2)).getActionLogs((Date)anyObject(), (Date)anyObject(), (String)eq(null), anyInt(), eq(20));

		
	}

	/**
	 * This helper method creates a list of 20 upload results.
	 * 
	 * @return
	 */
	private List<ActionLog> actionLogs() {

		List<ActionLog> actionLogs = new ArrayList<>();

		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, +16);

		final String configurationFilename = "some configurationFilename";
		final String filename = "some filename";
		final String filePath = "some/file/path";

		for (int i = 0; i < 20; i++) {

			ActionLog uploadResult = new ActionLog();
			uploadResult.setAction("upload");
			uploadResult.setConfigurationFileName(configurationFilename);
			uploadResult.setFilename(filename);
			uploadResult.setFolderName(filePath);
			uploadResult.setActionDurationInSeconds(100L);
			uploadResult.setRemoteServiceResponse("HTTP 200");
			uploadResult.setActionStartTime(cal.getTime());
			uploadResult.setActionSuccessful(true);
			actionLogs.add(uploadResult);

			cal.add(Calendar.DATE, -1);

		}

		return actionLogs;

	}

}
