/**
 * 
 */
package de.flexguse.images.uploadr.command.impl;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.junit.Before;
import org.junit.Test;

import de.flexguse.images.uploadr.command.UploadrCommandException;
import de.flexguse.images.uploadr.command.impl.UnlockCommand;
import de.flexguse.images.uploadr.service.LockInformationService;

/**
 * @author Christoph Guse, info@flexguse.de
 * 
 */
public class UnlockCommandTest {

	private UnlockCommand unlockCommand;

	@Before
	public void setUp() {
		unlockCommand = new UnlockCommand();
	}

	@Test
	public void testMissingCommandName() {

		try {
			unlockCommand.execute();
			fail("exception expected");
		} catch (UploadrCommandException e) {
			assertEquals(
					"Missing command name. For unlocking command the configuration filename and the command name is needed.",
					e.getMessage());
		}

	}

	@Test
	public void testUnlockNotLocked() throws UploadrCommandException {

		String unlockCommandName = "unlock";
		String configurationFileName = "someConfigurationFilename";
		String commandName = "someCommandName";
		String[] uploadrArguments = { unlockCommandName, configurationFileName, commandName };
		unlockCommand.setUploadrArgumentsQuietly(uploadrArguments);
		unlockCommand.setConfigurationFilename(configurationFileName);

		LockInformationService lockInformationService = mock(LockInformationService.class);
		when(
				lockInformationService.isLocked(commandName,
						configurationFileName)).thenReturn(false);
		unlockCommand.setLockInformationService(lockInformationService);

		try {
			unlockCommand.execute();
			fail("exception expected");
		} catch (UploadrCommandException e) {
			assertEquals(
					"The command 'someCommandName' for the configuration file 'someConfigurationFilename' is not locked and can't be unlocked.",
					e.getMessage());
		}

	}

	@Test
	public void testUnlock() throws UploadrCommandException {
		String unlockCommandName = "unlock";
		String configurationFileName = "someConfigurationFilename";
		String commandName = "someCommandName";
		String[] uploadrArguments = { unlockCommandName, configurationFileName, commandName };
		unlockCommand.setUploadrArgumentsQuietly(uploadrArguments);
		unlockCommand.setConfigurationFilename(configurationFileName);

		LockInformationService lockInformationService = mock(LockInformationService.class);
		when(
				lockInformationService.isLocked(commandName,
						configurationFileName)).thenReturn(true);
		unlockCommand.setLockInformationService(lockInformationService);
		
		unlockCommand.execute();
		verify(lockInformationService, times(1)).unlockCommand(commandName, configurationFileName);
		
		

	}

}
