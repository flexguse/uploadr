/**
 * 
 */
package de.flexguse.images.uploadr.command.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.springframework.context.ApplicationContext;

import de.flexguse.images.uploadr.command.UploadrCommandException;
import de.flexguse.images.uploadr.command.impl.UploadCommand;
import de.flexguse.images.uploadr.service.AlreadyLockedException;
import de.flexguse.images.uploadr.service.LockInformationService;
import de.flexguse.images.uploadr.service.UploadrService;
import de.flexguse.images.uploadr.service.flickr.FlickrUploadr;

/**
 * @author Christoph Guse, info@flexguse.de
 *
 */
public class UploadCommandTest {

	private UploadCommand uploadCommand;
		
	@Test
	public void testExecute() throws UploadrCommandException, AlreadyLockedException{
		
		final String configFile = "configuration/flickr_test.properties";
		
		String[] args = {"upload", configFile};
		uploadCommand = new UploadCommand();
		uploadCommand.setUploadrArguments(args);
		
		ApplicationContext springContext = mock(ApplicationContext.class);
		uploadCommand.setApplicationContext(springContext);
		
		UploadrService uploadrService = mock(UploadrService.class);
		when(springContext.getBean(UploadrService.class)).thenReturn(uploadrService);
		
		FlickrUploadr externalUploadr = mock(FlickrUploadr.class);
		
		FuzzyExternalUploadrHashMap externalUploadrMap = new FuzzyExternalUploadrHashMap();
		externalUploadrMap.put("flickr", externalUploadr);
		uploadCommand.setExternalUploadrMap(externalUploadrMap);
		
		when(springContext.getBean(FlickrUploadr.class)).thenReturn(externalUploadr);
		
		LockInformationService lockInformationService = mock(LockInformationService.class);
		when(lockInformationService.isLocked(anyString(), anyString())).thenReturn(false);
		uploadCommand.setLockInformationService(lockInformationService);
				
		uploadCommand.setApplicationContext(springContext);
		uploadCommand.execute();
		
		// check the expected behavior
		verify(lockInformationService, times(1)).lockCommand("upload", configFile);
		verify(uploadrService, times(1)).setExternalUploadr(externalUploadr);
		verify(uploadrService, times(1)).scanAndUpload(configFile, "/some/image/path");
		verify(lockInformationService, times(1)).unlockCommand("upload", configFile);
		
	}
	
	@Test
	public void testMissingConfigurationFile(){
		
		String[] args = {"upload"};
		try {
			uploadCommand = new UploadCommand();
			uploadCommand.setUploadrArguments(args);
			fail("exception expected");
		} catch (UploadrCommandException e) {
			assertEquals("no configuration file given", e.getMessage());
		}
	}
	
	@Test
	public void testAlreadyRunningException(){
		LockInformationService lockInformationService = mock(LockInformationService.class);
		when(lockInformationService.isLocked(anyString(), anyString())).thenReturn(true);

		
		String[] args = {"upload", "configuration/flickr.properties"};
		try {
			uploadCommand = new UploadCommand();
			uploadCommand.setUploadrArguments(args);
			uploadCommand.setLockInformationService(lockInformationService);
			uploadCommand.execute();
			fail("exception expected");
		} catch (UploadrCommandException e) {
			assertEquals("UploadCommand is already running for configuration/flickr.properties", e.getMessage());
		}
		
	}
	
	@Test
	public void testEmptyBasefolders() throws UploadrCommandException{
		
		final String configFile = "configuration/flickr_empty_basefolders.properties";
		
		String[] args = {"upload", configFile};
		uploadCommand = new UploadCommand();
		uploadCommand.setUploadrArguments(args);
		
		ApplicationContext springContext = mock(ApplicationContext.class);
		
		UploadrService uploadrService = mock(UploadrService.class);
		when(springContext.getBean(UploadrService.class)).thenReturn(uploadrService);
		
		FlickrUploadr externalUploadr = mock(FlickrUploadr.class);
		when(springContext.getBean(FlickrUploadr.class)).thenReturn(externalUploadr);
		
		LockInformationService lockInformationService = mock(LockInformationService.class);
		when(lockInformationService.isLocked(anyString(), anyString())).thenReturn(false);
		uploadCommand.setLockInformationService(lockInformationService);

		FuzzyExternalUploadrHashMap externalUploadrMap = new FuzzyExternalUploadrHashMap();
		externalUploadrMap.put("flickr", externalUploadr);
		uploadCommand.setExternalUploadrMap(externalUploadrMap);
		
		uploadCommand.setApplicationContext(springContext);
		try {
			uploadCommand.execute();
		} catch (UploadrCommandException e) {
			assertEquals("no upload basefolders given", e.getMessage());
		}

		
	}
	
}
