package de.flexguse.images.uploadr.command.impl;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.junit.Before;
import org.junit.Test;

import de.flexguse.images.uploadr.service.ExternalUploadr;

public class FuzzyExternalUploadrHashMapTest {

	private FuzzyExternalUploadrHashMap map;
	
	@Before
	public void setUp(){
		map = new FuzzyExternalUploadrHashMap();
	}
	
	@Test
	public void testGet(){
		
		assertNull(map.get(null));
		assertNull(map.get(""));
		assertNull(map.get("hello"));
		
		ExternalUploadr uploadr1 = mock(ExternalUploadr.class);
		
		map.put("flickr", uploadr1);
		
		ExternalUploadr fetched = map.get("flickr");
		assertEquals(uploadr1,fetched);
		assertEquals(uploadr1, map.get("configuration/flickr.properties"));
		assertEquals(uploadr1, map.get("flickr_test"));
		
		
	}
	
}
