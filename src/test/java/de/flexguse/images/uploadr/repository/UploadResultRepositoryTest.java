/**
 * 
 */
package de.flexguse.images.uploadr.repository;

import static org.junit.Assert.assertEquals;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import de.flexguse.images.uploadr.model.ActionLog;

/**
 * @author Christoph Guse, info@flexguse.de
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:/repository-test-context.xml")
public class UploadResultRepositoryTest {

	@Autowired
	private ActionLogRepository repository;

	@Rollback(true)
	@Transactional
	@Test
	public void testSaveUploadResult() {

		ActionLog result1 = new ActionLog();
		result1.setConfigurationFileName("file1");
		repository.save(result1);

		ActionLog result2 = new ActionLog();
		result2.setConfigurationFileName("file2");
		repository.save(result2);

		List<ActionLog> allResults = repository.findAll();
		assertEquals("2 UploadResults expected", 2, allResults.size());

	}

	@Rollback(true)
	@Transactional
	@Test
	public void testFindByKeys() {

		String configurationFilename1 = "configurationFilename1";
		String configurationFilename2 = "configurationFilename2";

		String imageFilepath = "imageFilepath";
		String imageFilename = "imageFilename";

		ActionLog result1 = new ActionLog();
		result1.setConfigurationFileName(configurationFilename1);
		result1.setFilename(imageFilename);
		result1.setFolderName(imageFilepath);
		repository.save(result1);

		ActionLog result2 = new ActionLog();
		result2.setConfigurationFileName(configurationFilename2);
		result2.setFilename(imageFilename);
		result2.setFolderName(imageFilepath);
		repository.save(result2);

		assertEquals(result1,
				repository.findOneByConfigurationFileNameAndFilenameAndFolderName(
								configurationFilename1, imageFilename,
								imageFilepath));
		assertEquals(result2,
				repository
						.findOneByConfigurationFileNameAndFilenameAndFolderName(
								configurationFilename2, imageFilename,
								imageFilepath));

	}

	@Rollback(true)
	@Transactional
	@Test
	public void testFindLatest() {

		final String configurationFilename = "someConfigurationFilename";
		final String filePath = "/some/filename/path";

		ActionLog result1 = new ActionLog();
		result1.setConfigurationFileName(configurationFilename);
		result1.setFolderName(filePath);
		result1.setActionStartTime(new Date());
		repository.save(result1);

		ActionLog result2 = new ActionLog();
		result2.setConfigurationFileName(configurationFilename);
		result2.setFolderName(filePath);
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.HOUR_OF_DAY, -1);
		result2.setActionStartTime(cal.getTime());
		repository.save(result2);

		// List<UploadResult> uploadResults =
		// repository.findLatest(configurationFilename, filePath);
		// assertEquals("only the newest UploadResult is expected", 1,
		// uploadResults.size());
		// assertEquals(result1, uploadResults.get(0));
	}

}
