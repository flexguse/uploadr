/**
 * 
 */
package de.flexguse.images.uploadr.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import de.flexguse.images.uploadr.model.UploadedFile;

/**
 * @author Christoph Guse, info@flexguse.de
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:/repository-test-context.xml")
public class UploadedFileRepositoryTest {

	@Autowired
	private UploadedFileRepository repository;

	@Transactional
	@Rollback(true)
	@Test
	public void testGetUploadedFilenames() {

		assertTrue(repository.getUploadedFilenames(null, null).isEmpty());
		assertTrue(repository.getUploadedFilenames("some/file/path", null)
				.isEmpty());
		assertTrue(repository.getUploadedFilenames(null, "some.conf").isEmpty());

		final String filepath = "D:\\Users\\Development\\common-playground\\uploadr\\target\\test-classes\\images\\test2";
		final String configurationFilename = "flickr.properties";
		final String configurationFilename2 = "other.properties";
		final String filename1 = "firstFile.jpg";
		final String filename2 = "secondFile.jpg";
		final String filename3 = "thirdFile.jpg";
		final String filename4 = "fourthFile.jpg";
		
		UploadedFile file1 = createUploadedFile(filename1, filepath, configurationFilename);
		repository.save(file1);
		UploadedFile file2 = createUploadedFile(filename2, filepath, configurationFilename);
		repository.save(file2);
		UploadedFile file3 = createUploadedFile(filename3, filepath, configurationFilename);
		repository.save(file3);
		UploadedFile file4 = createUploadedFile(filename4, filepath, configurationFilename2);
		repository.save(file4);
		
		// the folder paths are stored as URIs to be platform independent
		List<String> check1 = repository.getUploadedFilenames(new File(filepath).toURI().toString(), configurationFilename);
		assertEquals("3 entries expected for flickr.properties", 3, check1.size());
		assertTrue(check1.contains(filename1));
		assertTrue(check1.contains(filename2));
		assertTrue(check1.contains(filename3));
		
		// the folder paths are stored as URIs to be platform independent
		List<String> check2 = repository.getUploadedFilenames(new File(filepath).toURI().toString(), configurationFilename2);
		assertEquals("only 1 entry expected for other.properties", 1, check2.size());
		assertTrue(check2.contains(filename4));
		
	}

	/**
	 * This helper method creates a {@link UploadedFile} object.
	 * 
	 * @param filename
	 * @param filepath
	 * @param configurationFilename
	 * @return
	 */
	private UploadedFile createUploadedFile(String filename, String filepath,
			String configurationFilename) {

		UploadedFile file = new UploadedFile();
		file.setConfigurationFilename(configurationFilename);
		file.setFilePath(filepath);
		file.setFilename(filename);
		return file;

	}

}
