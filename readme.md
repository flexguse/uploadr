# Introduction

Often you have the common situation: locally (that means in your local network) you have lots of images and you would like to share them. 
Unfortunately your internet connection is slow and you don't want to keep your PC running to wait for a finished upload (at least in my case).

Often there are Network Attached Storages (NAS) available but there is no service which is able to synchronize you photo service.

Uploadr is a multi purpose uploader command line tool which is easy to extend and provides detailed logging information.

## Implemeted photo services
Currently there is only support for http://www.flickr.com implemented. 

# Uploadr usage

Uploadr contains three layers:
	- the Uploadr core which is able to call 'Commands'
	- the 'Commands' which call 'ExternalUploadrs'
	- the 'ExternalUploadr' which does the work
	
Change to the bin folder of the ZIP file and run
	
	:::java
	uploadr {command} {command options...}
	
## Available commands
It is important to know only one unique combination of command and configuration is allowed to run at once. 

### help
Shows all available commands.

### status
Shows all running commands.

### unlock
Only one combination of command and configuration is allowed to run at once. This is implemented by using a lock file. In some 
cases the command could be finished but the lockfile is not deleted. Using this command manually unlocks the command.

Use 'help' command to get more information.

### logging
Uploadr runs in the background, so it is very useful to have advanced logging. Currently the logging command shows all successful 
uploads and all failed uploads. Logging output can be filtered by date and configuration filename.

Use 'help' command to get more information.

### oauth
Flickr provides OAuth authentication. The authentication process is somehow a pain, therefore this was automated.

Use 'help' command to get more information. 

### upload
Starts the upload. Use 'help' command to get more information.


# FAQ

## Where are the configuration files located?
All configuration files for the Uploadr must be located in the 'conf' folder.

## The Uploadr ZIP is quite big, why?
Yes, Uploadr is no lean utility, the ZIP is quite big. This is due to the Enterprise Technology which was used to implement Uploadr:
	- Spring Framework
	- Spring Data JPA Framework
	- QueryDSL Framework
	- Hibernate
	- H2 Database
	- etc.
	
## Starting a command is quite slow!
Well, it takes some time to initialize Spring and the Database. But in contrast to the upload process which may last about hours the 
startup time should not be too bad.

## How do I build the application and what do I need for that?
To build Uploadr from the sources you need Maven 3 and Java 1.7 installed.

Run 

	:::java
	mvn clean package

in your command line. This starts the compilation, executes the JUnit tests and creates the ZIP file containing the Uploadr binaries.

But you don't need to build Uploadr yourself, you can download the binary distribution on this page.

## What are the System Requirements to run Uploadr?
The base requirement is an installed JRE of Java 1.7. This is available for many Platforms, even ARM.
Uploadr is configured to use 64Megs of RAM at maximum (hopefully), so your running system should provide this. I tested Uploadr on my
Synolgy 212j NAS and it worked well. I think there shouldn't be any problem to run Uploadr on a Raspberry PI or a similar system.

## Where do I get more information?
Have a look at the Wiki.

## Something is broken and Uploadr does not work properly!
Please create an issue in the issues list.
